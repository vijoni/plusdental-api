<?php

return [
  'database' => [
    'host' => '127.0.0.1',
    'port' => 5432,
    'database' => 'vijoni_shop',
    'user' => 'vijoni_shop_user',
    'password' => 'root',
    'master_url' => 'pgsql://vijoni_shop_user:root@127.0.0.1:5432/vijoni_shop',
    'slave_url' => 'pgsql://vijoni_shop_user:root@127.0.0.1:5432/vijoni_shop',
  ],
  'redis' => [
    'host' => '127.0.0.1',
    'port' => 6379,
    'database' => 0,
  ],
];
