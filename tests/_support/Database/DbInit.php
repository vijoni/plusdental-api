<?php

declare(strict_types=1);

namespace VijoniTest\Database;

use DateTime;
use Vijoni\Database\Client\DatabaseClient;
use Vijoni\Database\Client\PgDatabaseClient;
use Vijoni\Unit\AppConfig;

class DbInit
{
  public function createTestDatabase(string $owner): string
  {
    $dbConfig = $this->readDatabaseConfig();
    $testDatabaseName = $this->createDatabase($dbConfig, $owner);
    $this->grantAccessToDatabase($dbConfig, $testDatabaseName, $owner);

    return $testDatabaseName;
  }

  private function readDatabaseConfig(): AppConfig
  {
    $configProperties = require(__DIR__ . '/../../../config/_generated/config.php');
    $appConfig = new AppConfig($configProperties);

    return $appConfig->extractConfig('database');
  }

  private function createDatabase(AppConfig $dbConfig, string $owner): string
  {
    $dbClient = $this->createDatabaseClient($dbConfig, $dbConfig->getString('database'));
    $dbClient->init();

    $dateSuffix = (new DateTime())->format('YmdHis_u');
    $testDatabaseName = 'database_shop_test_' . $dateSuffix . '_' . rand();

    $query = <<<SQL
CREATE DATABASE {$testDatabaseName}
ENCODING=UTF8
LC_COLLATE='en_US.utf8'
LC_CTYPE='en_US.utf8'
OWNER={$owner}
TEMPLATE=template0;
SQL;

    $dbClient->queryNative($query, "create test database [{$testDatabaseName}]");

    return $testDatabaseName;
  }

  private function grantAccessToDatabase(AppConfig $dbConfig, string $database, string $owner): void
  {
    $dbClient = $this->createDatabaseClient($dbConfig, $database);
    $dbClient->init();

    $query = "ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON TABLES TO {$owner};";
    $dbClient->queryNative($query, 'grant test database access on all tables');

    $query = "ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON SEQUENCES TO {$owner};";
    $dbClient->queryNative($query, 'grant test database access on all sequences');

    $query = "ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON FUNCTIONS TO {$owner};";
    $dbClient->queryNative($query, 'grant test database access on all functions');
  }

  private function createDatabaseClient(AppConfig $dbConfig, string $database): DatabaseClient
  {
    validatePrivilegedDatabaseCredentials();

    $dbUrl = buildDbUrl(
      $dbConfig->getString('host'),
      $dbConfig->getInt('port'),
      $database,
      DB_PRIVILEGED_USER,
      DB_PRIVILEGED_PASSWORD
    );

    return new PgDatabaseClient($dbUrl, $dbUrl);
  }
}
