<?php

declare(strict_types=1);

namespace VijoniTest\Integration\Application\SchemaValidator;

use Codeception\Test\Unit;
use Vijoni\Application\SchemaValidator\JsonSchemaValidator;

class JsonSchemaValidatorTest extends Unit
{
  private const SCHEMA_FILE_PATH = __DIR__ . '/fixtures/schema.json';

  public function testManyErrorsReturn(): void
  {
    $payload = '{}';
    $validator = new JsonSchemaValidator();
    $result = $validator->validate($payload, self::SCHEMA_FILE_PATH);

    $rootErrors = $result['/'];
    $this->assertCount(2, $rootErrors);
  }

  public function testReturnedRootErrorIdentifiers(): void
  {
    $payload = '{}';
    $validator = new JsonSchemaValidator();
    $result = $validator->validate($payload, self::SCHEMA_FILE_PATH);

    $rootErrors = $result['/'];
    $this->assertSame('REQUIRED_FIELD::firstname', $this->readError($rootErrors, 0));
    $this->assertSame('REQUIRED_FIELD', $this->readErrorCode($rootErrors, 0));
    $this->assertSame('firstname', $this->readErrorField($rootErrors, 0));

    $this->assertSame('REQUIRED_FIELD::email', $this->readError($rootErrors, 1));
    $this->assertSame('REQUIRED_FIELD', $this->readErrorCode($rootErrors, 1));
    $this->assertSame('email', $this->readErrorField($rootErrors, 1));
  }

  public function testReturnedFieldErrorIdentifiers(): void
  {
    $payload = '{"firstname": 21, "email": "konrad.gawlinski@gmail.com"}';
    $validator = new JsonSchemaValidator();
    $result = $validator->validate($payload, self::SCHEMA_FILE_PATH);

    $errors = $result['/firstname'];
    $this->assertSame('MUST_BE_A_STRING::firstname', $this->readError($errors, 0));
    $this->assertSame('MUST_BE_A_STRING', $this->readErrorCode($errors, 0));
    $this->assertSame('firstname', $this->readErrorField($errors, 0));
  }

  public function testFieldCustomErrorMessage(): void
  {
    $expectedMessage = 'Firstname must be a text value';
    $payload = '{"firstname": 21, "email": "konrad.gawlinski@gmail.com"}';
    $validator = new JsonSchemaValidator();
    $result = $validator->validate($payload, self::SCHEMA_FILE_PATH, [
      'MUST_BE_A_STRING::firstname' => $expectedMessage
    ]);

    $errors = $result['/firstname'];
    $this->assertSame($expectedMessage, $errors[0]['message']);
  }

  public function testFieldCustomErrorMessageWithArgument(): void
  {
    $payload = '{"firstname": "Kon", "email": "konrad.gawlinski@gmail.com"}';
    $validator = new JsonSchemaValidator();
    $result = $validator->validate($payload, self::SCHEMA_FILE_PATH, [
      'TOO_SHORT::firstname' => 'Firstname must be longer than {min} characters',
    ]);

    $errors = $result['/firstname'];
    $this->assertSame('Firstname must be longer than 5 characters', $errors[0]['message']);
  }

  private function readError(array $errors, int $index): string
  {
    return $this->readRootProperty($errors, $index, 'error');
  }

  private function readErrorCode(array $errors, int $index): string
  {
    return $this->readRootProperty($errors, $index, 'errorCode');
  }

  private function readErrorField(array $errors, int $index): string
  {
    return $this->readRootProperty($errors, $index, 'errorField');
  }

  private function readRootProperty(array $errors, int $index, string $key): string
  {
    return $errors[$index][$key];
  }
}
