#!/usr/bin/php
<?php

declare(strict_types=1);

use Vijoni\Application;
use Vijoni\Unit\AppConfig;

require_once __DIR__ . '/../src/bootstrap.php';
require_once __DIR__ . '/../src/internal_functions.php';

class LiquibaseCommand
{
  private AppConfig $dbConfig;

  public function __construct(private AppConfig $appConfig)
  {
    $this->dbConfig = $appConfig->extractConfig('database');
  }

  public function init(): void
  {
    $app = new Application($this->appConfig);
    $app->initDependencyProvider();
  }

  public function run(string $liquibaseCommand): void
  {
    $dbConnectionPath = $this->dbConnectionPath();

    $changelogFilePaths = $this->findChangelogPaths();
    foreach ($changelogFilePaths as $path) {
      echo "\n\n-----------------\n\n";
      echo coloredString("Run migration for [{$path}]:\n\n");

      system($this->liquibaseCommand($dbConnectionPath, $liquibaseCommand, $path), $exitCode);

      if ($exitCode !== 0) {
        exit($exitCode);
      }
    }
  }

  private function findChangelogPaths(): array
  {
    $filePaths = glob(__DIR__ . '/../src/*/_database/changelog.xml', GLOB_ERR);
    $filePaths = array_merge([__DIR__ . '/../src/_database/changelog.xml'], $filePaths);

    return array_map('realpath', $filePaths);
  }

  private function dbConnectionPath(): string
  {
    $dbConfig = $this->dbConfig;
    $host = $dbConfig->getString('host');
    $port = $dbConfig->getString('port');
    $dbName = $dbConfig->getString('database');

    return sprintf('jdbc:postgresql://%s:%s/%s?connect_timeout=5', $host, $port, $dbName);
  }

  private function liquibaseCommand(string $dbConnectionPath, string $command, string $changelogFilePath): string
  {
    validatePrivilegedDatabaseCredentials();
    $user = DB_PRIVILEGED_USER;
    $password= DB_PRIVILEGED_PASSWORD;

    return <<<CMD
liquibase --driver=org.postgresql.Driver \
  --classpath=/ \
  --changeLogFile={$changelogFilePath} \
  --url="{$dbConnectionPath}" \
  --username={$user} \
  --password={$password} \
  --liquibaseSchemaName=public \
  {$command}
CMD;
  }
}
