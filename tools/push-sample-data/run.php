#!/usr/bin/php
<?php

declare(strict_types=1);

use Vijoni\Unit\AppConfig;

require __DIR__ . '/../../src/bootstrap.php';

function coloredString(string $input): string {
  return "\033[0;32m". $input . "\033[0m";
}

class Command
{
  public function run(): void
  {
    $configProperties = require(__DIR__ . '/../../config/_generated/config.php');
    $appConfig = new AppConfig($configProperties);

    $dbConfig = $appConfig->extractConfig('database');

    $host = $dbConfig->getString('host');
    $port = $dbConfig->getString('port');
    $dbName = $dbConfig->getString('database');
    $user = $dbConfig->getString('user');
    $password = $dbConfig->getString('password');

    $changelogFilePaths = $this->findSampleDataSourceFiles();
    foreach ($changelogFilePaths as $path) {
      echo coloredString("Push data for [{$path}]:\n");
      echo "-----------------\n\n";

      system("PGPASSWORD='{$password}' psql -h {$host} -p {$port} -d {$dbName} -U {$user} -f {$path}");
    }
  }

  private function findSampleDataSourceFiles(): array
  {
    $filePaths = glob(__DIR__ . '/../../src/*/_database/sample-data.sql', GLOB_ERR);

    return array_map('realpath', $filePaths);
  }
}

$cmd = new Command();
$cmd->run();
