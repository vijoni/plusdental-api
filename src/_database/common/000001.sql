-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE OR REPLACE FUNCTION updated_at_timestamp_column()
    RETURNS TRIGGER
    LANGUAGE plpgsql
AS '
    BEGIN
        NEW.updated_at = now();
        RETURN NEW;
    END;
';

-- changeset kgawlinski:002
-- remove a nested property
CREATE OR REPLACE FUNCTION jsonb_minus ( arg1 jsonb, arg2 jsonb )
    RETURNS jsonb
AS $$
SELECT
    COALESCE(json_object_agg(key,
         CASE
             WHEN jsonb_typeof(value) = 'object' AND arg2 -> key IS NOT NULL THEN jsonb_minus(value, arg2 -> key)
             ELSE value
             END
     ), '{}')::jsonb
FROM jsonb_each(arg1)
WHERE arg1 -> key <> arg2 -> key OR arg2 -> key IS NULL
$$ LANGUAGE SQL;

CREATE OPERATOR - (
    PROCEDURE = jsonb_minus,
    LEFTARG   = jsonb,
    RIGHTARG  = jsonb
);

-- changeset kgawlinski:003
CREATE OR REPLACE FUNCTION trigger_create_set_owner()
    RETURNS event_trigger
    LANGUAGE plpgsql
AS '
DECLARE
    obj record;
BEGIN
    FOR obj IN SELECT * FROM pg_event_trigger_ddl_commands() WHERE command_tag=''CREATE TABLE'' LOOP
        EXECUTE format(''ALTER TABLE %s OWNER TO vijoni_shop_user'', obj.object_identity);
    END LOOP;
END;
';

-- changeset kgawlinski:004
CREATE EVENT TRIGGER trigger_create_set_owner
    ON ddl_command_end
    WHEN tag IN ('CREATE TABLE')
EXECUTE PROCEDURE trigger_create_set_owner();

-- changeset kgawlinski:005
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
