<?php

declare(strict_types=1);

define('DB_PRIVILEGED_USER', getenv('DB_PRIVILEGED_USER') ?: 'undefined');
define('DB_PRIVILEGED_PASSWORD', getenv('DB_PRIVILEGED_PASSWORD') ?: 'undefined');

function validatePrivilegedDatabaseCredentials(): void
{
  if (DB_PRIVILEGED_USER === 'undefined' || DB_PRIVILEGED_PASSWORD === 'undefined') {
    throw new \Exception('Required env variables are missing: [DB_PRIVILEGED_USER, DB_PRIVILEGED_PASSWORD]');
  }
}

function buildDbUrl(string $host, int $port, string $database, string $user, string $pass): string
{
  return sprintf('pgsql://%s:%s@%s:%s/%s', $user, $pass, $host, $port, $database);
}

function coloredString(string $input): string
{
  return "\033[0;32m" . $input . "\033[0m";
}
