<?php

declare(strict_types=1);

namespace Vijoni;

use Vijoni\Application\DependencyProvider\DependencyProviderKeys;
use Vijoni\Application\Http\Router;
use Vijoni\Application\Redis\RedisClient;
use Vijoni\Application\SchemaValidator\JsonSchemaValidator;
use Vijoni\Database\Client\PgDatabaseClient;
use Vijoni\Unit\AppConfig;
use Vijoni\Unit\DependencyProvider;

class Application
{
  private DependencyProvider $dependencyProvider;

  public function __construct(AppConfig $appConfig)
  {
    $this->dependencyProvider = DependencyProvider::getInstance();
    $this->dependencyProvider->setConfig($appConfig);
  }

  public function initDependencyProvider(): void
  {
    $this->initMoney();
    $this->registerJsonSchemaValidator();
    $this->registerMainDatabase();
    $this->registerCacheStorage();
  }

  public function dispatch(): void
  {
    $router = new Router();
    $router->initAndDispatch();
  }

  private function initMoney(): void
  {
    bcscale(16);
  }

  private function registerJsonSchemaValidator(): void
  {
    $this->dependencyProvider->register(
      DependencyProviderKeys::JSON_SCHEMA_VALIDATOR,
      static fn () => new JsonSchemaValidator()
    );
  }

  private function registerMainDatabase(): void
  {
    $dbConfig = $this->dependencyProvider->getConfig()->extractConfig('database');
    $dbClient = new PgDatabaseClient($dbConfig->getString('master_url'), $dbConfig->getString('slave_url'));

    $this->dependencyProvider->register(
      DependencyProviderKeys::MAIN_DATABASE,
      static function () use ($dbClient) {
        $dbClient->init();

        return $dbClient;
      }
    );
  }

  private function registerCacheStorage(): void
  {
    $redisConfig = $this->dependencyProvider->getConfig()->extractConfig('redis');
    $redisClient = new RedisClient(
      $redisConfig->getString('host'),
      $redisConfig->getInt('port'),
      $redisConfig->getInt('database'),
    );

    $this->dependencyProvider->register(
      DependencyProviderKeys::CACHE_STORAGE,
      static function () use ($redisClient) {
        $redisClient->init();

        return $redisClient;
      }
    );
  }
}
