<?php

declare(strict_types=1);

namespace Vijoni\Application\Redis;

class RedisException extends \Exception
{
}
