<?php

declare(strict_types=1);

namespace Vijoni\Application\DependencyProvider;

interface DependencyProviderKeys
{
  public const JSON_SCHEMA_VALIDATOR = 'json_schema_validator';
  public const MAIN_DATABASE = 'main_database';
  public const CACHE_STORAGE = 'cache_storage';
}
