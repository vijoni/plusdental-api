<?php

declare(strict_types=1);

namespace Vijoni\Application\Http;

use Nyholm\Psr7\Response;

// @phpstan-ignore-next-line
class JsonResponse extends Response
{
  public function __construct(
    int $status = 200,
    array $headers = [],
    $body = null,
    string $version = '1.1',
    string $reason = null
  ) {
    $headers['Content-Type'] = 'application/json';

    parent::__construct($status, $headers, $body, $version, $reason);
  }
}
