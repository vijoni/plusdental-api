<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog;

use Vijoni\ProductCatalog\Catalog\ModuleFacade as CatalogFacade;
use Vijoni\ProductCatalog\Shared\Product;
use Vijoni\ProductCatalog\Shared\ProductCollection;
use Vijoni\ProductCatalog\Shared\Region;
use Vijoni\Unit\BaseUnitFacade;

class UnitFacade extends BaseUnitFacade
{
  public function findProductsByIdByRegion(array $productIds, string $countryCode, string $locale): array
  {
    $region = new Region($countryCode, $locale);

    /** @var CatalogFacade $moduleFacade */
    $moduleFacade = $this->dependencyProvider()->shareModuleFacade(CatalogFacade::class);
    $products = $moduleFacade->findProductsByIdByRegion($productIds, $region);

    return $this->exposeProducts($products);
  }

  private function exposeProducts(ProductCollection $products): array
  {
    $exposedProducts = [];
    foreach ($products as $product) {
      $exposedProducts[$product->getProductId()] = $product->toDeepArray();
    }

    return $exposedProducts;
  }
}
