<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Shared;

class ProductCollection extends \ArrayIterator
{
  /**
   * @param Product[] $products
   * @param int $flags
   */
  public function __construct(array $products = [], int $flags = 0)
  {
    parent::__construct($products, $flags);
  }

  /**
   * @param Product $value
   * @return void
   */
  public function append(mixed $value): void
  {
    throw new \RuntimeException('This collection can be indexed only by strings. Use `offsetSet` instead');
  }

  /**
   * @return Product
   */
  public function current(): mixed
  {
    /** @var Product */
    return parent::current();
  }

  /**
   * @param string $key
   * @return Product
   */
  public function offsetGet(mixed $key): mixed
  {
    /** @var Product */
    return parent::offsetGet($key);
  }

  /**
   * @param string $key
   * @param Product $value
   * @return void
   */
  public function offsetSet(mixed $key, mixed $value): void
  {
    parent::offsetSet($key, $value);
  }

  /**
   * @param string $key
   * @return void
   */
  public function offsetUnset(mixed $key): void
  {
    parent::offsetUnset($key);
  }

  public function toDeepArray(): array
  {
    $asArray = [];
    foreach ($this as $productId => $product) {
      $asArray[$productId] = $product->toDeepArray();
    }

    return $asArray;
  }

  public function sumPrices(): string
  {
    $sumPrice = '';
    foreach ($this as $product) {
      $count = (string)$product->getCount();
      $totalPrice = bcmul($product->getPrice(), $count);
      $sumPrice = bcadd($sumPrice, $totalPrice);
    }

    return $sumPrice;
  }
}
