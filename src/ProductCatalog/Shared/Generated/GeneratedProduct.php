<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Shared\Generated;

use Vijoni\ClassGenerator\ClassBase;

class GeneratedProduct extends ClassBase
{
  public const TYPE_SIMPLE = 'simple';
  public const TYPE_SET = 'set';
  public const TYPE_CONFIG = 'config';

  private string $productId = '';
  private bool $isActive =  false;
  private string $name = '';
  private string $type = '';
  private string $price = '';
  private string $taxRateType = '';
  private string $displayName = '';
  private int $count =  1;
  private \Vijoni\ProductCatalog\Shared\ProductChildCollection $children;
  private \Vijoni\ProductCatalog\Shared\ProductCollection $childProducts;

  public const PRODUCT_ID = 'productId';
  public const IS_ACTIVE = 'isActive';
  public const NAME = 'name';
  public const TYPE = 'type';
  public const PRICE = 'price';
  public const TAX_RATE_TYPE = 'taxRateType';
  public const DISPLAY_NAME = 'displayName';
  public const COUNT = 'count';
  public const CHILDREN = 'children';
  public const CHILD_PRODUCTS = 'childProducts';

  public function getProductId(): string
  {
    return $this->productId;
  }

  public function setProductId(string $productId): void
  {
    $this->productId = $productId;
    $this->modified[self::PRODUCT_ID] = true;
  }

  public function getIsActive(): bool
  {
    return $this->isActive;
  }

  public function setIsActive(bool $isActive): void
  {
    $this->isActive = $isActive;
    $this->modified[self::IS_ACTIVE] = true;
  }

  public function getName(): string
  {
    return $this->name;
  }

  public function setName(string $name): void
  {
    $this->name = $name;
    $this->modified[self::NAME] = true;
  }

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): void
  {
    $this->type = $type;
    $this->modified[self::TYPE] = true;
  }

  public function getPrice(): string
  {
    return $this->price;
  }

  public function setPrice(string $price): void
  {
    $this->price = $price;
    $this->modified[self::PRICE] = true;
  }

  public function getTaxRateType(): string
  {
    return $this->taxRateType;
  }

  public function setTaxRateType(string $taxRateType): void
  {
    $this->taxRateType = $taxRateType;
    $this->modified[self::TAX_RATE_TYPE] = true;
  }

  public function getDisplayName(): string
  {
    return $this->displayName;
  }

  public function setDisplayName(string $displayName): void
  {
    $this->displayName = $displayName;
    $this->modified[self::DISPLAY_NAME] = true;
  }

  public function getCount(): int
  {
    return $this->count;
  }

  public function setCount(int $count): void
  {
    $this->count = $count;
    $this->modified[self::COUNT] = true;
  }

  public function getChildren(): \Vijoni\ProductCatalog\Shared\ProductChildCollection
  {
    return $this->children;
  }

  public function setChildren(\Vijoni\ProductCatalog\Shared\ProductChildCollection $children): void
  {
    $this->children = $children;
    $this->modified[self::CHILDREN] = true;
  }

  public function getChildProducts(): \Vijoni\ProductCatalog\Shared\ProductCollection
  {
    return $this->childProducts;
  }

  public function setChildProducts(\Vijoni\ProductCatalog\Shared\ProductCollection $childProducts): void
  {
    $this->childProducts = $childProducts;
    $this->modified[self::CHILD_PRODUCTS] = true;
  }

  public function toArray(): array
  {
    return [
      self::PRODUCT_ID => $this->getProductId(),
      self::IS_ACTIVE => $this->getIsActive(),
      self::NAME => $this->getName(),
      self::TYPE => $this->getType(),
      self::PRICE => $this->getPrice(),
      self::TAX_RATE_TYPE => $this->getTaxRateType(),
      self::DISPLAY_NAME => $this->getDisplayName(),
      self::COUNT => $this->getCount(),
      self::CHILDREN => $this->getChildren(),
      self::CHILD_PRODUCTS => $this->getChildProducts(),
    ];
  }

  public function toDeepArray(): array
  {
    return $this->toArray();
  }

  public function fromArray(array $properties): void
  {
    isset($properties[self::PRODUCT_ID]) && $this->setProductId($properties[self::PRODUCT_ID]);
    isset($properties[self::IS_ACTIVE]) && $this->setIsActive($properties[self::IS_ACTIVE]);
    isset($properties[self::NAME]) && $this->setName($properties[self::NAME]);
    isset($properties[self::TYPE]) && $this->setType($properties[self::TYPE]);
    isset($properties[self::PRICE]) && $this->setPrice($properties[self::PRICE]);
    isset($properties[self::TAX_RATE_TYPE]) && $this->setTaxRateType($properties[self::TAX_RATE_TYPE]);
    isset($properties[self::DISPLAY_NAME]) && $this->setDisplayName($properties[self::DISPLAY_NAME]);
    isset($properties[self::COUNT]) && $this->setCount($properties[self::COUNT]);
    isset($properties[self::CHILDREN]) && $this->setChildren($properties[self::CHILDREN]);
    isset($properties[self::CHILD_PRODUCTS]) && $this->setChildProducts($properties[self::CHILD_PRODUCTS]);
  }

  public function fieldKeys(): array
  {
    return [
      self::PRODUCT_ID => true,
      self::IS_ACTIVE => true,
      self::NAME => true,
      self::TYPE => true,
      self::PRICE => true,
      self::TAX_RATE_TYPE => true,
      self::DISPLAY_NAME => true,
      self::COUNT => true,
      self::CHILDREN => true,
      self::CHILD_PRODUCTS => true,
    ];
  }
}
