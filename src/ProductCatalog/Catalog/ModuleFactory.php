<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Catalog;

use Vijoni\ProductCatalog\Catalog\Repository\MapperFactory;
use Vijoni\ProductCatalog\Catalog\Repository\ProductReadRepository;
use Vijoni\ProductCatalog\Catalog\UseCase\FindProductsUseCase;
use Vijoni\Unit\BaseModuleFactory;

/**
 * @method ModuleDependencyProvider dependencyProvider()
 * @method ModuleConfig config()
 */
class ModuleFactory extends BaseModuleFactory
{
  public function newFindProductsUseCase(): FindProductsUseCase
  {
    return new FindProductsUseCase($this->shareCatalogReadRepository());
  }

  private function shareCatalogReadRepository(): ProductReadRepository
  {
    /** @var ProductReadRepository */
    return $this->share(
      ProductReadRepository::class,
      fn () => new ProductReadRepository(
        $this->dependencyProvider()->shareDatabaseClient(),
        $this->shareMapperFactory()
      )
    );
  }

  private function shareMapperFactory(): MapperFactory
  {
    /** @var MapperFactory */
    return $this->share(
      MapperFactory::class,
      fn () => new MapperFactory()
    );
  }
}
