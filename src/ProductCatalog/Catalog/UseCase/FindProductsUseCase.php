<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Catalog\UseCase;

use Vijoni\ProductCatalog\Catalog\Repository\ProductReadRepository;
use Vijoni\ProductCatalog\Shared\Product;
use Vijoni\ProductCatalog\Shared\ProductCollection;
use Vijoni\ProductCatalog\Shared\Region;

class FindProductsUseCase
{
  public function __construct(private ProductReadRepository $catalogRepository)
  {
  }

  public function findProductsByIdByRegion(array $productIds, Region $region): ProductCollection
  {
    $productCollection = $this->catalogRepository->findProductsByIdByRegion($productIds, $region);
    $allChildIds = $this->extractAllChildIds($productCollection);

    if (empty($allChildIds)) {
      return $productCollection;
    }

    $childProducts = $this->catalogRepository->findProductsByIdByRegion($allChildIds, $region);
    $this->fillProductsWithChildProducts($productCollection, $childProducts);
    $this->calculateSetPriceFromItsChildren($productCollection);

    return $productCollection;
  }

  private function extractAllChildIds(ProductCollection $productCollection): array
  {
    $allChildIds = [];
    /** @var Product $product */
    foreach ($productCollection as $product) {
      $allChildIds += $product->getChildren()->allProductIds();
    }

    return array_unique($allChildIds);
  }

  private function fillProductsWithChildProducts(
    ProductCollection $products,
    ProductCollection $discoveredChildProducts
  ): void {
    foreach ($products as $product) {
      $childProducts = $this->lookupChildProducts($product, $discoveredChildProducts);
      $product->setChildProducts($childProducts);
    }
  }

  private function lookupChildProducts(Product $parentProduct, ProductCollection $products): ProductCollection
  {
    $children = $parentProduct->getChildren();

    $childProducts = new ProductCollection();
    foreach ($children as $productChild) {
      $productId = $productChild->getProductId();
      $product = $products[$productId]->clone();
      $product->setCount($productChild->getCount());

      $childProducts[$productId] = $product;
    }

    return $childProducts;
  }

  private function calculateSetPriceFromItsChildren(ProductCollection $products): void
  {
    foreach ($products as $product) {
      if ($product->isTypeSet()) {
        $price = $product->getChildProducts()->sumPrices();
        $product->setPrice(roundCents($price));
      }
    }
  }
}
