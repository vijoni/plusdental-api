<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Catalog\Http;

use Vijoni\Application\Http\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vijoni\ProductCatalog\Catalog\ModuleFactory;
use Vijoni\ProductCatalog\Shared\Region;
use Vijoni\Unit\ModuleAction;

/**
 * @method ModuleFactory moduleFactory()
 */
class FindProductAction extends ModuleAction
{
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    /** @var string $productId */
    $productId = $request->getAttribute('productId');

    /** @var string $countryCode */
    $countryCode = $request->getAttribute('countryCode');

    /** @var string $locale */
    $locale = $request->getAttribute('locale');

    $region = new Region($countryCode, $locale);

    $useCase = $this->moduleFactory()->newFindProductsUseCase();
    $products = $useCase->findProductsByIdByRegion([$productId], $region);

    if (!$products->valid()) {
      return new JsonResponse(404, [], '[]');
    }

    $products->rewind();
    $product = $products->current();

    return new JsonResponse(200, [], own_json_encode($product->toDeepArray()));
  }
}
