<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Catalog;

use Vijoni\Application\DependencyProvider\DatabaseClient;
use Vijoni\Application\DependencyProvider\JsonSchemaValidator;
use Vijoni\Unit\BaseModuleDependencyProvider;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
class ModuleDependencyProvider extends BaseModuleDependencyProvider
{
  use JsonSchemaValidator;
  use DatabaseClient;
}
