<?php

declare(strict_types=1);

namespace Vijoni\ProductCatalog\Catalog\Repository;

class MapperFactory
{
  public function newProductMapper(): ProductMapper
  {
    return new ProductMapper();
  }
}
