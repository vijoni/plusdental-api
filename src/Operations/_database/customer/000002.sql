-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE operations_address(
   dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
   address_id UUID NOT NULL UNIQUE DEFAULT uuid_generate_v1mc(),
   firstname VARCHAR NOT NULL,
   lastname VARCHAR NOT NULL,
   phone VARCHAR NOT NULL,
   street VARCHAR NOT NULL,
   address_complement VARCHAR,
   city VARCHAR NOT NULL,
   post_code VARCHAR NOT NULL,
   country_code VARCHAR NOT NULL,
   created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
   updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changeset kgawlinski:002
CREATE INDEX IF NOT EXISTS operations_address__address_id ON operations_address USING BTREE (address_id);

-- changset kgawlinski:003
CREATE TRIGGER update_operations_address_updated_at
    BEFORE UPDATE ON operations_address
    FOR EACH ROW
EXECUTE PROCEDURE updated_at_timestamp_column();
