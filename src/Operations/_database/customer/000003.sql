-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE operations_customer_address(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    customer_dbid UUID NOT NULL REFERENCES operations_customer(dbid),
    address_dbid UUID NOT NULL REFERENCES operations_address(dbid),
    type VARCHAR NOT NULL DEFAULT '',
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changeset kgawlinski:002
CREATE INDEX IF NOT EXISTS operations_customer_address__customer_id ON operations_customer_address USING BTREE (customer_dbid);
CREATE INDEX IF NOT EXISTS operations_customer_address__address_id ON operations_customer_address USING BTREE (address_dbid);

-- changset kgawlinski:003
CREATE TRIGGER update_operations_customer_address_updated_at
    BEFORE UPDATE ON operations_customer_address
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
