<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer;

use Vijoni\Unit\BaseModuleConfig;

class ModuleConfig extends BaseModuleConfig
{
}
