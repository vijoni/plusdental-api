<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\UseCase;

use Vijoni\Operations\Customer\Repository\CustomerReadRepository;
use Vijoni\Operations\Shared\Address;

class FindAddressesUseCase
{
  public function __construct(private CustomerReadRepository $customerRepository)
  {
  }

  /**
   * @return Address[]
   */
  public function findAddressesByIds(array $addressIds): array
  {
    return $this->customerRepository->findAddressesByIds($addressIds);
  }
}
