<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer;

use Vijoni\Operations\Customer\Repository\CustomerReadRepository;
use Vijoni\Operations\Customer\Repository\CustomerReadWriteRepository;
use Vijoni\Operations\Customer\Repository\CustomerWriteRepository;
use Vijoni\Operations\Customer\Repository\MapperFactory;
use Vijoni\Operations\Customer\Request\CreateCustomerRequest;
use Vijoni\Operations\Customer\Request\UpdateCustomerRequest;
use Vijoni\Operations\Customer\UseCase\CreateCustomerUseCase;
use Vijoni\Operations\Customer\UseCase\FindAddressesUseCase;
use Vijoni\Operations\Customer\UseCase\FindCustomerAddressesUseCase;
use Vijoni\Operations\Customer\UseCase\UpdateCustomerUseCase;
use Vijoni\Operations\Shared\ValidationMessagesMap;
use Vijoni\Operations\Shared\ValidationMessagesMapEN;
use Vijoni\Unit\BaseModuleFactory;

/**
 * @method ModuleDependencyProvider dependencyProvider()
 * @method ModuleConfig config()
 */
class ModuleFactory extends BaseModuleFactory
{
  public function newCreateCustomerRequest(): CreateCustomerRequest
  {
    return new CreateCustomerRequest(
      $this->dependencyProvider()->shareJsonSchemaValidator(),
      $this->newValidationMessagesMap()
    );
  }

  public function newUpdateCustomerRequest(): UpdateCustomerRequest
  {
    return new UpdateCustomerRequest(
      $this->dependencyProvider()->shareJsonSchemaValidator(),
      $this->newValidationMessagesMap()
    );
  }

  private function newValidationMessagesMap(): ValidationMessagesMap
  {
    return new ValidationMessagesMapEN();
  }

  public function newCreateCustomerUseCase(): CreateCustomerUseCase
  {
    return new CreateCustomerUseCase($this->shareCustomerRepository());
  }

  public function newUpdateCustomerUseCase(): UpdateCustomerUseCase
  {
    return new UpdateCustomerUseCase($this->shareCustomerRepository());
  }

  public function newFindAddressesUseCase(): FindAddressesUseCase
  {
    return new FindAddressesUseCase($this->shareCustomerReadRepository());
  }

  public function newFindCustomerAddressesUseCase(): FindCustomerAddressesUseCase
  {
    return new FindCustomerAddressesUseCase($this->shareCustomerReadRepository());
  }

  private function shareCustomerRepository(): CustomerReadWriteRepository
  {
    /** @var CustomerReadWriteRepository */
    return $this->share(
      CustomerReadWriteRepository::class,
      fn () => new CustomerReadWriteRepository(
        $this->shareCustomerReadRepository(),
        $this->shareCustomerWriteRepository()
      )
    );
  }

  private function shareCustomerWriteRepository(): CustomerWriteRepository
  {
    /** @var CustomerWriteRepository */
    return $this->share(
      CustomerWriteRepository::class,
      fn () => new CustomerWriteRepository(
        $this->dependencyProvider()->shareDatabaseClient(),
        $this->shareMapperFactory()
      )
    );
  }

  private function shareCustomerReadRepository(): CustomerReadRepository
  {
    /** @var CustomerReadRepository */
    return $this->share(
      CustomerReadRepository::class,
      fn () => new CustomerReadRepository(
        $this->dependencyProvider()->shareDatabaseClient(),
        $this->shareMapperFactory()
      )
    );
  }

  private function shareMapperFactory(): MapperFactory
  {
    /** @var MapperFactory */
    return $this->share(
      MapperFactory::class,
      fn () => new MapperFactory()
    );
  }
}
