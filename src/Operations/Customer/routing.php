<?php

declare(strict_types=1);

use Slim\SlimApp;
use Vijoni\Operations\Customer\Http\CreateCustomerAction;
use Vijoni\Operations\Customer\Http\UpdateCustomerAction;

return function (SlimApp $slimApp): void {
  $slimApp->post('/', CreateCustomerAction::class)->setName('create-customer');
  $slimApp->patch('/', UpdateCustomerAction::class)->setName('updated-customer');
};
