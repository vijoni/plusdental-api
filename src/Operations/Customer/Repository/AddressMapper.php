<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Repository;

use Vijoni\Operations\Shared\Address;
use Vijoni\Operations\Shared\AddressDbTable;
use Vijoni\Operations\Shared\CustomerAddressDbTable;

class AddressMapper
{
  /**
   * @return Address[]
   */
  public function mapDbRowsToAddresses(\Traversable $rows): array
  {
    return iter_map($rows, $this->mapDbRowToAddress(...));
  }

  public function mapDbRowToAddress(array $row): Address
  {
    $address = new Address();
    $address->fromArray([
      Address::ADDRESS_ID => $row[AddressDbTable::ADDRESS_ID],
      Address::FIRSTNAME => $row[AddressDbTable::FIRSTNAME],
      Address::LASTNAME => $row[AddressDbTable::LASTNAME],
      Address::PHONE => $row[AddressDbTable::PHONE],
      Address::STREET => $row[AddressDbTable::STREET],
      Address::ADDRESS_COMPLEMENT => $row[AddressDbTable::ADDRESS_COMPLEMENT],
      Address::CITY => $row[AddressDbTable::CITY],
      Address::POST_CODE => $row[AddressDbTable::POST_CODE],
      Address::COUNTRY_CODE => $row[AddressDbTable::COUNTRY_CODE],
      Address::TYPE => $row[CustomerAddressDbTable::TYPE],
    ]);

    return $address;
  }
}
