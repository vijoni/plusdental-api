<?php

declare(strict_types=1);

namespace Vijoni\Operations\Customer\Repository;

use Vijoni\Operations\Shared\Customer;
use Vijoni\Operations\Shared\CustomerDbTable;
use Vijoni\Database\Client\DatabaseClient;

class CustomerWriteRepository
{
  public function __construct(private DatabaseClient $db, private MapperFactory $mapperFactory)
  {
    $db->useWriteConnection();
  }

  public function createCustomer(Customer $customer): void
  {
    $customerMapper = $this->mapperFactory->newCustomerMapper();
    $customerProperties = $customerMapper->mapCustomerToDbRow($customer);

    $qb = $this->db->newQueryBuilder(
      'INSERT INTO operations_customer (%lc) VALUES(%l?) RETURNING %c;',
      [
        array_keys($customerProperties),
        array_values($customerProperties),
        CustomerDbTable::CUSTOMER_ID
      ],
      [],
      "operations: create customer; email:[{$customer->getEmail()}]"
    );

    $newCustomerId = (string)$this->db->queryStringValue($qb);
    $customer->setCustomerId($newCustomerId);
  }

  public function updateCustomer(Customer $customer): void
  {
    $customerMapper = $this->mapperFactory->newCustomerMapper();
    $customerProperties = $customerMapper->mapCustomerToDbRow($customer);

    $qb = $this->db->newQueryBuilder(
      'UPDATE operations_customer SET %lu WHERE %c=%s',
      [
        $customerProperties,
        CustomerDbTable::CUSTOMER_ID,
        $customer->getCustomerId(),
      ],
      [],
      "operations: update customer; customerId:[{$customer->getCustomerId()}]"
    );

    $this->db->query($qb);
  }
}
