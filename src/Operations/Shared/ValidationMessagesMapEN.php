<?php

declare(strict_types=1);

namespace Vijoni\Operations\Shared;

class ValidationMessagesMapEN implements ValidationMessagesMap
{
  public function readMap(): array
  {
    return [
      'REQUIRED_FIELD::firstname' => 'Firstname is required',
      'MUST_BE_A_STRING::firstname' => 'Firstname must be a text value',
      'TOO_SHORT::firstname' => 'Firstname has to be longer than {min} characters',
      'TOO_LONG::firstname' => 'Firstname has to be shorter than {max} characters',
      'PATTERN_MISMATCH::firstname' => 'Firstname cannot contain special characters',

      'REQUIRED_FIELD::lastname' => 'Lastname is required',
      'MUST_BE_A_STRING::lastname' => 'Lastname must be a text value',
      'TOO_SHORT::lastname' => 'Lastname has to be longer than {min} characters',
      'TOO_LONG::lastname' => 'Lastname has to be shorter than {max} characters',
      'PATTERN_MISMATCH::lastname' => 'Lastname cannot contain special characters',

      'REQUIRED_FIELD::email' => 'Email is required',
      'MUST_BE_A_STRING::email' => 'Email must be a text value',
      'TOO_LONG::email' => 'Email has to be shorter than {max} characters',

      'REQUIRED_FIELD::birthDate' => 'Birth date is required',
      'INVALID_DATE_FORMAT::birthDate' => 'Date must match specific format, eg. 1984-10-13',
    ];
  }
}
