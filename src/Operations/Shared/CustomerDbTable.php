<?php

declare(strict_types=1);

namespace Vijoni\Operations\Shared;

class CustomerDbTable
{
  public const CUSTOMER_ID = 'customer_id';
  public const FIRSTNAME = 'firstname';
  public const LASTNAME = 'lastname';
  public const EMAIL = 'email';
  public const BIRTH_DATE = 'birth_date';
}
