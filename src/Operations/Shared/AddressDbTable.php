<?php

declare(strict_types=1);

namespace Vijoni\Operations\Shared;

class AddressDbTable
{
  public const ADDRESS_ID = 'address_id';
  public const FIRSTNAME = 'firstname';
  public const LASTNAME = 'lastname';
  public const PHONE = 'phone';
  public const STREET = 'street';
  public const ADDRESS_COMPLEMENT = 'address_complement';
  public const CITY = 'city';
  public const POST_CODE = 'post_code';
  public const COUNTRY_CODE = 'country_code';
}
