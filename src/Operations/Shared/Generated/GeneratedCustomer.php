<?php

declare(strict_types=1);

namespace Vijoni\Operations\Shared\Generated;

use Vijoni\ClassGenerator\ClassBase;

class GeneratedCustomer extends ClassBase
{
  public const DEFAULT_DATE = '1900-01-01 00:00:00';

  private string $customerId = '';
  private string $firstname = '';
  private string $lastname = '';
  private string $email = '';
  private \DateTime $birthDate;

  public const CUSTOMER_ID = 'customerId';
  public const FIRSTNAME = 'firstname';
  public const LASTNAME = 'lastname';
  public const EMAIL = 'email';
  public const BIRTH_DATE = 'birthDate';

  public function getCustomerId(): string
  {
    return $this->customerId;
  }

  public function setCustomerId(string $customerId): void
  {
    $this->customerId = $customerId;
    $this->modified[self::CUSTOMER_ID] = true;
  }

  public function getFirstname(): string
  {
    return $this->firstname;
  }

  public function setFirstname(string $firstname): void
  {
    $this->firstname = $firstname;
    $this->modified[self::FIRSTNAME] = true;
  }

  public function getLastname(): string
  {
    return $this->lastname;
  }

  public function setLastname(string $lastname): void
  {
    $this->lastname = $lastname;
    $this->modified[self::LASTNAME] = true;
  }

  public function getEmail(): string
  {
    return $this->email;
  }

  public function setEmail(string $email): void
  {
    $this->email = $email;
    $this->modified[self::EMAIL] = true;
  }

  public function getBirthDate(): \DateTime
  {
    return $this->birthDate;
  }

  public function setBirthDate(\DateTime $birthDate): void
  {
    $this->birthDate = $birthDate;
    $this->modified[self::BIRTH_DATE] = true;
  }

  public function toArray(): array
  {
    return [
      self::CUSTOMER_ID => $this->getCustomerId(),
      self::FIRSTNAME => $this->getFirstname(),
      self::LASTNAME => $this->getLastname(),
      self::EMAIL => $this->getEmail(),
      self::BIRTH_DATE => $this->getBirthDate(),
    ];
  }

  public function toDeepArray(): array
  {
    return $this->toArray();
  }

  public function fromArray(array $properties): void
  {
    isset($properties[self::CUSTOMER_ID]) && $this->setCustomerId($properties[self::CUSTOMER_ID]);
    isset($properties[self::FIRSTNAME]) && $this->setFirstname($properties[self::FIRSTNAME]);
    isset($properties[self::LASTNAME]) && $this->setLastname($properties[self::LASTNAME]);
    isset($properties[self::EMAIL]) && $this->setEmail($properties[self::EMAIL]);
    isset($properties[self::BIRTH_DATE]) && $this->setBirthDate($properties[self::BIRTH_DATE]);
  }

  public function fieldKeys(): array
  {
    return [
      self::CUSTOMER_ID => true,
      self::FIRSTNAME => true,
      self::LASTNAME => true,
      self::EMAIL => true,
      self::BIRTH_DATE => true,
    ];
  }
}
