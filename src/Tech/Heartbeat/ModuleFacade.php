<?php

declare(strict_types=1);

namespace Vijoni\Tech\Heartbeat;

use Vijoni\Unit\BaseModuleFacade;

/**
 * @method ModuleFactory moduleFactory()
 */
class ModuleFacade extends BaseModuleFacade
{
}
