<?php

declare(strict_types=1);

use Slim\SlimApp;
use Vijoni\Tech\Heartbeat\Http\HeartbeatAction;

return function (SlimApp $slimApp): void {
  $slimApp->get('/', HeartbeatAction::class);
};
