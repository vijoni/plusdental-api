<?php

declare(strict_types=1);

namespace Vijoni\Tech\Heartbeat;

use Vijoni\Unit\BaseModuleDependencyProvider;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
class ModuleDependencyProvider extends BaseModuleDependencyProvider
{
}
