-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE sales_tax_rate(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    country_code VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    value NUMERIC NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changeset kgawlinski:002
CREATE INDEX IF NOT EXISTS sales_tax_rate__country_code ON sales_tax_rate USING BTREE (country_code);
CREATE INDEX IF NOT EXISTS sales_tax_rate__type ON sales_tax_rate USING BTREE (type);

-- changset kgawlinski:003
CREATE TRIGGER update_sales_tax_rate_updated_at
    BEFORE UPDATE ON sales_tax_rate
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
