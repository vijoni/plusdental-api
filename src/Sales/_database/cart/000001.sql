-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE sales_cart_item(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    customer_id UUID NOT NULL UNIQUE,
    product_id UUID NOT NULL,
    price NUMERIC NOT NULL,
    count INT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changeset kgawlinski:002
CREATE INDEX IF NOT EXISTS sales_cart_item__productid ON sales_cart_item USING BTREE (product_id);

-- changset kgawlinski:003
CREATE TRIGGER update_sales_cart_item_updated_at
    BEFORE UPDATE ON sales_cart_item
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
