-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE sales_order(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    order_id UUID NOT NULL UNIQUE DEFAULT uuid_generate_v1mc(),
    customer_id UUID NOT NULL,
    purchase_id UUID,
    price_to_pay NUMERIC NOT NULL,
    net_price NUMERIC NOT NULL,
    tax_value NUMERIC NOT NULL,
    discount NUMERIC DEFAULT 0,
    voucher_code VARCHAR,
    country_code VARCHAR NOT NULL,
    locale VARCHAR NOT NULL,
    status VARCHAR NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changeset kgawlinski:002
CREATE INDEX IF NOT EXISTS sales_order__customer_id ON sales_order USING BTREE (customer_id);
CREATE INDEX IF NOT EXISTS sales_order__created_at ON sales_order USING BTREE (created_at);

-- changset kgawlinski:003
CREATE TRIGGER update_sales_order_updated_at
    BEFORE UPDATE ON sales_order
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
