-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE sales_order_address(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    order_dbid UUID NOT NULL REFERENCES sales_order(dbid),
    firstname VARCHAR NOT NULL,
    lastname VARCHAR NOT NULL,
    phone VARCHAR NOT NULL,
    street VARCHAR NOT NULL,
    address_complement VARCHAR,
    city VARCHAR NOT NULL,
    post_code VARCHAR NOT NULL,
    country_code VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changset kgawlinski:002
CREATE TRIGGER update_sales_order_address_updated_at
    BEFORE UPDATE ON sales_order_address
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
