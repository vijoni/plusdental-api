-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE sales_order_totals(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    order_dbid UUID NOT NULL REFERENCES sales_order(dbid),
    price_to_pay NUMERIC NOT NULL,
    net_price NUMERIC NOT NULL,
    tax_value NUMERIC NOT NULL,
    tax_rate NUMERIC,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changset kgawlinski:003
CREATE TRIGGER update_sales_order_totals_updated_at
    BEFORE UPDATE ON sales_order_totals
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
