-- liquibase formatted sql

-- changeset kgawlinski:001
CREATE TABLE sales_order_item(
    dbid UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
    order_dbid UUID NOT NULL REFERENCES sales_order(dbid),
    product_id UUID NOT NULL,
    parent_product_id UUID,
    product_type VARCHAR NOT NULL,
    group_id UUID,
    price_to_pay NUMERIC NOT NULL,
    net_price NUMERIC NOT NULL,
    tax_value NUMERIC NOT NULL,
    tax_rate NUMERIC,
    discount NUMERIC DEFAULT 0,
    count INT NOT NULL,
    ordered_count INT NOT NULL,
    total_price_to_pay NUMERIC NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

-- changset kgawlinski:002
CREATE INDEX IF NOT EXISTS sales_order_item__product_id ON sales_order_item USING BTREE (product_id);
CREATE INDEX IF NOT EXISTS sales_order_item__product_type ON sales_order_item USING BTREE (product_type);
CREATE INDEX IF NOT EXISTS sales_order_item__group_id ON sales_order_item USING BTREE (group_id);

-- changset kgawlinski:003
CREATE TRIGGER update_sales_order_item_updated_at
    BEFORE UPDATE ON sales_order_item
    FOR EACH ROW
    EXECUTE PROCEDURE updated_at_timestamp_column();
