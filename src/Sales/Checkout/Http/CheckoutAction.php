<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Http;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vijoni\Application\Http\JsonResponse;
use Vijoni\Sales\Checkout\ModuleFactory;
use Vijoni\Unit\ModuleAction;

/**
 * @method ModuleFactory moduleFactory()
 */
class CheckoutAction extends ModuleAction
{
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $moduleFactory = $this->moduleFactory();

    /** @var string $customerId */
    $customerId = $request->getAttribute('customerId');
    $checkoutRequest = $moduleFactory->newCheckoutRequest();
    $checkoutRequest->use($request->getBody()->getContents());
    $errors = $checkoutRequest->validate();

    if (!empty($errors)) {
      return new JsonResponse(400, [], own_json_encode($errors));
    }

    $orderItems = $checkoutRequest->createOrderItems();
    $region = $checkoutRequest->readRegion();
    $useCase = $moduleFactory->newCheckoutUseCase();
    $userCaseResult = $useCase->checkout($customerId, $orderItems, $region);

    if ($userCaseResult->hasFailed()) {
      return new JsonResponse(422, [], own_json_encode($userCaseResult->errorsToArray()));
    }

    return new JsonResponse(200, [], own_json_encode($userCaseResult->getCheckoutData()));
  }
}
