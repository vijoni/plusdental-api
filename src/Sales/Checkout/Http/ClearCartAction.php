<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Http;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vijoni\Application\Http\JsonResponse;
use Vijoni\Sales\Checkout\ModuleFactory;
use Vijoni\Unit\ModuleAction;

/**
 * @method ModuleFactory moduleFactory()
 */
class ClearCartAction extends ModuleAction
{
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $moduleFactory = $this->moduleFactory();
    /** @var string $customerId */
    $customerId = $request->getAttribute('customerId');

    $moduleFactory->newClearCartUseCase()->clearCart($customerId);

    return new JsonResponse(200, [], '{}');
  }
}
