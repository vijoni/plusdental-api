<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Http;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vijoni\Application\Http\JsonResponse;
use Vijoni\Sales\Checkout\ModuleFactory;
use Vijoni\Unit\ModuleAction;

/**
 * @method ModuleFactory moduleFactory()
 */
class UpdateCartAction extends ModuleAction
{
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $moduleFactory = $this->moduleFactory();

    /** @var string $customerId */
    $customerId = $request->getAttribute('customerId');

    $updateCartRequest = $moduleFactory->newUpdateCartRequest();
    $updateCartRequest->use($customerId, $request->getBody()->getContents());
    $errors = $updateCartRequest->validate();

    if (!empty($errors)) {
      return new JsonResponse(400, [], own_json_encode($errors));
    }

    $orderItem = $updateCartRequest->createOrderItem();
    $moduleFactory->newUpdateCartUseCase()->updateOrderItem($customerId, $orderItem);

    return new JsonResponse(200, [], '{}');
  }
}
