<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Http;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vijoni\Application\Http\JsonResponse;
use Vijoni\Sales\Checkout\ModuleFactory;
use Vijoni\Sales\Checkout\UseCase\PurchaseUseCase;
use Vijoni\Sales\Shared\Result\PurchaseUseCaseResult;
use Vijoni\Unit\ModuleAction;

/**
 * @method ModuleFactory moduleFactory()
 */
class PurchaseAction extends ModuleAction
{
  public function __invoke(ServerRequestInterface $request): ResponseInterface
  {
    $moduleFactory = $this->moduleFactory();

    /** @var string $customerId */
    $customerId = $request->getAttribute('customerId');
    $purchaseRequest = $moduleFactory->newPurchaseRequest();
    $purchaseRequest->use($customerId, $request->getBody()->getContents());
    $errors = $purchaseRequest->validate();

    if (!empty($errors)) {
      return new JsonResponse(400, [], own_json_encode($errors));
    }

    $order = $purchaseRequest->createOrder();
    $useCase = $moduleFactory->newPurchaseUseCase();
    $result = $useCase->purchase($order, uuidv4());
    if ($result->hasFailed()) {
      return $this->buildUseCaseErrorResponse($result);
    }

    return new JsonResponse(200, [], own_json_encode($result->getDetails()));
  }

  private function buildUseCaseErrorResponse(PurchaseUseCaseResult $result): JsonResponse
  {
    $errorCode = $result->getFirstError()->getErrorCode();
    $httpReturnCodeMap = [
      PurchaseUseCase::PURCHASE_CHECKOUT_REQUEST_MISMATCH => 422,
      PurchaseUseCase::DUPLICATE_ORDER => 409
    ];

    if (isset($httpReturnCodeMap[$errorCode])) {
      return new JsonResponse($httpReturnCodeMap[$errorCode], [], own_json_encode($result->errorsToArray()));
    }

    throw new \Exception('Purchase use case unsupported error code');
  }
}
