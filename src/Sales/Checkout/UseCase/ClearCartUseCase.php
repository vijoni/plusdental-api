<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\UseCase;

use Vijoni\Sales\Checkout\Repository\CheckoutWriteRepository;

class ClearCartUseCase
{
  public function __construct(private CheckoutWriteRepository $cartRepository)
  {
  }

  public function clearCart(string $customerId): void
  {
    $this->cartRepository->clearCustomersCart($customerId);
  }
}
