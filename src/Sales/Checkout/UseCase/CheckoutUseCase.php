<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\UseCase;

use Vijoni\Application\Error;
use Vijoni\Sales\Checkout\Service\CheckoutChecksum;
use Vijoni\Sales\Checkout\Service\CheckoutReaderAggregate;
use Vijoni\Sales\Finance\ModuleFacade as FinanceFacade;
use Vijoni\Sales\Shared\Address;
use Vijoni\Sales\Shared\InstallmentPayment;
use Vijoni\Sales\Shared\OrderItem;
use Vijoni\Sales\Shared\Payment;
use Vijoni\Sales\Shared\Product;
use Vijoni\Sales\Shared\Region;
use Vijoni\Sales\Shared\Result\CheckoutUseCaseResult;
use Vijoni\Sales\Shared\Totals;

class CheckoutUseCase
{
  public const PRODUCT_PRICES_MISMATCH = 'PRODUCT_PRICES_MISMATCH';

  public function __construct(
    private CheckoutReaderAggregate $checkoutReader,
    private CheckoutChecksum $checkoutChecksum,
    private FinanceFacade $financeFacade
  ) {
  }

  /**
   * @param OrderItem[] $orderItems
   */
  public function checkout(string $customerId, array $orderItems, Region $region): CheckoutUseCaseResult
  {
    $products = $this->checkoutReader->getProductReader()->readProducts($orderItems, $region);
    $result = $this->verifyOrderItemsPrices($orderItems, $products);
    if ($result->hasFailed()) {
      return $result;
    }

    $this->checkoutReader->getProductReader()->mergeOrderItemsPropertiesIntoProducts($orderItems, $products);
    $this->financeFacade->calculateAllProductSums($products);
    $taxRateTotals = $this->financeFacade->calculateTotalsPerTaxRate($products);
    $totals = $this->financeFacade->calculateTotalsFromTaxRateTotals($taxRateTotals);

    $payments = $this->checkoutReader->getPaymentReader()->readAvailablePayments($products, $totals);
    $customerAddresses = $this->checkoutReader->getAddressReader()->readAvailableAddresses($customerId);
    $checksum = $this->checkoutChecksum->createChecksum($customerId, $products);
    $this->checkoutChecksum->storeChecksum($customerId, $checksum);

    $result = new CheckoutUseCaseResult(true);
    $result->setCheckoutData([
      'addresses' => $this->addressesToArray($customerAddresses),
      'products' => $this->productsToArray($products),
      'payments' => [
        'providers' => $this->paymentsToArray($payments, $totals),
      ],
      'taxRateTotals' => $this->taxRateTotalsToArray($taxRateTotals),
      'totals' => $totals->toArray(),
    ]);

    return $result;
  }

  /**
   * @param Product[] $products
   */
  private function productsToArray(array $products): array
  {
    return array_map(static fn(Product $product) => $product->toDeepArray(), $products);
  }

  /**
   * @param Payment[] $payments
   */
  private function paymentsToArray(array $payments, Totals $totals): array
  {
    $result = [];
    foreach ($payments as $payment) {
      $paymentAsArray = $payment->toArray();

      if ($payment instanceof InstallmentPayment) {
        $variantsTotals = $payment->calculateAllVariants($totals);
        $paymentAsArray['variants'] = array_map(fn (Totals $totals) => $totals->toArray(), $variantsTotals);
      }

      $result[] = $paymentAsArray;
    }

    return $result;
  }

  /**
   * @param Address[] $addresses
   */
  private function addressesToArray(array $addresses): array
  {
    return array_map(static fn(Address $address) => $address->toArray(), $addresses);
  }

  /**
   * @param Totals[] $taxRateTotals
   * @return array
   */
  private function taxRateTotalsToArray(array $taxRateTotals): array
  {
    $result = [];
    foreach ($taxRateTotals as $taxRate => $totals) {
      $result[$taxRate] = $totals->toArray();
    }

    return $result;
  }

  /**
   * @param OrderItem[] $orderItems
   * @param Product[] $products
   */
  private function verifyOrderItemsPrices(array $orderItems, array $products): CheckoutUseCaseResult
  {
    $mismatchingItems = [];
    foreach ($orderItems as $orderItem) {
      $product = $products[$orderItem->getProductId()];

      if ($product->getPriceToPay() !== $orderItem->getPrice()) {
        $mismatchingItems[$orderItem->getProductId()] = [
          'productId' => $orderItem->getProductId(),
          'price' => $orderItem->getPrice(),
          'currentPrice' => $product->getPriceToPay(),
        ];
      }
    }

    if ($mismatchingItems) {
      return new CheckoutUseCaseResult(false, [new Error(self::PRODUCT_PRICES_MISMATCH, $mismatchingItems)]);
    }

    return new CheckoutUseCaseResult(true);
  }
}
