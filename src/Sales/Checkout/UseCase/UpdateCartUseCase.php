<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\UseCase;

use Vijoni\Sales\Checkout\Repository\CheckoutReadWriteRepository;
use Vijoni\Sales\Shared\OrderItem;

class UpdateCartUseCase
{
  public function __construct(private CheckoutReadWriteRepository $cartRepository)
  {
  }

  public function updateOrderItem(string $customerId, OrderItem $orderItem): void
  {
    $readRepository = $this->cartRepository->read();
    $existingOrderItem = $readRepository->findOrderItemByCustomerIdAndProductId(
      $customerId,
      $orderItem->getProductId()
    );

    if (!$existingOrderItem->hasProductId()) {
      $this->cartRepository->write()->createCartItem($customerId, $orderItem);
    } else {
      $this->cartRepository->write()->updateCartItem($customerId, $orderItem);
    }
  }
}
