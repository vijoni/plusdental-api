<?php

declare(strict_types=1);

use Slim\SlimApp;
use Vijoni\Sales\Checkout\Http\CheckoutAction;
use Vijoni\Sales\Checkout\Http\ClearCartAction;
use Vijoni\Sales\Checkout\Http\PurchaseAction;
use Vijoni\Sales\Checkout\Http\UpdateCartAction;

return function (SlimApp $slimApp): void {
  $slimApp->post('/{customerId}/cart/update', UpdateCartAction::class)->setName('update-cart');
  $slimApp->post('/{customerId}/cart/clear', ClearCartAction::class)->setName('clear-cart');

  $slimApp->post('/{customerId}/checkout', CheckoutAction::class)->setName('checkout');
  $slimApp->post('/{customerId}/purchase', PurchaseAction::class)->setName('purchase');
};
