<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout;

use Vijoni\Application\DependencyProvider\CacheStorageClient;
use Vijoni\Application\DependencyProvider\DatabaseClient;
use Vijoni\Application\DependencyProvider\JsonSchemaValidator;
use Vijoni\Application\DependencyProvider\OperationsUnitFacade;
use Vijoni\Application\DependencyProvider\ProductCatalogUnitFacade;
use Vijoni\Sales\Finance\ModuleFacade as FinanceFacade;
use Vijoni\Sales\Order\ModuleFacade as OrderFacade;
use Vijoni\Unit\BaseModuleDependencyProvider;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
class ModuleDependencyProvider extends BaseModuleDependencyProvider
{
  use JsonSchemaValidator;
  use DatabaseClient;
  use CacheStorageClient;
  use ProductCatalogUnitFacade;
  use OperationsUnitFacade;

  /**
   * @return FinanceFacade
   */
  public function shareFinanceFacade(): FinanceFacade
  {
    /** @var FinanceFacade */
    return $this->dependencyProvider()->shareModuleFacade(FinanceFacade::class);
  }

  /**
   * @return OrderFacade
   */
  public function shareOrderFacade(): OrderFacade
  {
    /** @var OrderFacade */
    return $this->dependencyProvider()->shareModuleFacade(OrderFacade::class);
  }
}
