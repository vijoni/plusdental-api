<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Repository;

use Vijoni\Sales\Shared\OrderItem;
use Vijoni\Database\Client\DatabaseClient;

class CheckoutReadRepository
{
  public function __construct(private DatabaseClient $db, private MapperFactory $mapperFactory)
  {
  }

  public function findOrderItemByCustomerIdAndProductId(string $customerId, string $productId): OrderItem
  {
    $qb = $this->db->newQueryBuilder(
      'SELECT * FROM sales_cart_item WHERE customer_id = %s AND product_id = %s',
      [$customerId, $productId],
      [],
      "sales: find cart item by customerId and productId; customerId:[{$customerId}], productId:[{$productId}]"
    );
    $result = $this->db->querySingleRow($qb);

    if ($result->notFound()) {
      return new OrderItem();
    }

    $orderItemMapper = $this->mapperFactory->newOrderItemMapper();

    return $orderItemMapper->mapDbRowToOrderItem((array)$result->getValue());
  }

  /**
   * @param string $customerId
   * @return OrderItem[]
   */
  public function findOrderItemsByCustomerId(string $customerId): array
  {
    $qb = $this->db->newQueryBuilder(
      'SELECT * FROM sales_cart_item WHERE customer_id = %s',
      [$customerId],
      [],
      "sales: find all cart items by customerId; customerId:[{$customerId}]"
    );

    $orderItems = $this->db->query($qb);

    $orderItemMapper = $this->mapperFactory->newOrderItemMapper();

    return $orderItemMapper->mapDbRowsToOrderItems($orderItems);
  }
}
