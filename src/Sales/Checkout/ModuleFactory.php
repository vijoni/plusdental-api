<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout;

use Vijoni\Sales\Checkout\Repository\CheckoutCacheRepository;
use Vijoni\Sales\Checkout\Repository\CheckoutReadRepository;
use Vijoni\Sales\Checkout\Repository\CheckoutReadWriteRepository;
use Vijoni\Sales\Checkout\Repository\CheckoutWriteRepository;
use Vijoni\Sales\Checkout\Repository\MapperFactory;
use Vijoni\Sales\Checkout\Request\CheckoutRequest;
use Vijoni\Sales\Checkout\Request\PurchaseRequest;
use Vijoni\Sales\Checkout\Service\CheckoutAddressReader;
use Vijoni\Sales\Checkout\Service\CheckoutChecksum;
use Vijoni\Sales\Checkout\Service\CheckoutPaymentReader;
use Vijoni\Sales\Checkout\Service\CheckoutProductReader;
use Vijoni\Sales\Checkout\Service\CheckoutReaderAggregate;
use Vijoni\Sales\Checkout\UseCase\CheckoutUseCase;
use Vijoni\Sales\Checkout\UseCase\ClearCartUseCase;
use Vijoni\Sales\Checkout\UseCase\PurchaseUseCase;
use Vijoni\Sales\Checkout\UseCase\UpdateCartUseCase;
use Vijoni\Sales\Shared\ValidationMessagesMap;
use Vijoni\Sales\Shared\ValidationMessagesMapEN;
use Vijoni\Sales\Checkout\Request\UpdateCartRequest;
use Vijoni\Unit\BaseModuleFactory;

/**
 * @method ModuleDependencyProvider dependencyProvider()
 * @method ModuleConfig config()
 */
class ModuleFactory extends BaseModuleFactory
{
  public function newUpdateCartRequest(): UpdateCartRequest
  {
    return new UpdateCartRequest(
      $this->dependencyProvider()->shareJsonSchemaValidator(),
      $this->newValidationMessagesMap()
    );
  }

  public function newCheckoutRequest(): CheckoutRequest
  {
    return new CheckoutRequest(
      $this->dependencyProvider()->shareJsonSchemaValidator(),
      $this->newValidationMessagesMap()
    );
  }

  public function newPurchaseRequest(): PurchaseRequest
  {
    return new PurchaseRequest(
      $this->dependencyProvider()->shareJsonSchemaValidator(),
      $this->newValidationMessagesMap(),
      $this->dependencyProvider()->shareFinanceFacade(),
      $this->dependencyProvider()->shareOperationsUnitFacade()
    );
  }

  public function newUpdateCartUseCase(): UpdateCartUseCase
  {
    return new UpdateCartUseCase($this->shareCheckoutRepository());
  }

  public function newClearCartUseCase(): ClearCartUseCase
  {
    return new ClearCartUseCase($this->shareCheckoutWriteRepository());
  }

  public function newCheckoutUseCase(): CheckoutUseCase
  {
    return new CheckoutUseCase(
      $this->newCheckoutReaderAggregate(),
      $this->newCheckoutChecksum(),
      $this->dependencyProvider()->shareFinanceFacade()
    );
  }

  public function newPurchaseUseCase(): PurchaseUseCase
  {
    return new PurchaseUseCase(
      $this->newCheckoutReaderAggregate(),
      $this->newCheckoutChecksum(),
      $this->dependencyProvider()->shareFinanceFacade(),
      $this->dependencyProvider()->shareOrderFacade()
    );
  }

  private function newCheckoutReaderAggregate(): CheckoutReaderAggregate
  {
    return new CheckoutReaderAggregate(
      $this->newCheckoutProductReader(),
      $this->newCheckoutPaymentReader(),
      $this->newCheckoutAddressReader()
    );
  }

  private function newCheckoutProductReader(): CheckoutProductReader
  {
    return new CheckoutProductReader(
      $this->dependencyProvider()->shareProductCatalogUnitFacade(),
      $this->dependencyProvider()->shareFinanceFacade()
    );
  }

  private function newCheckoutPaymentReader(): CheckoutPaymentReader
  {
    return new CheckoutPaymentReader(
      $this->dependencyProvider()->shareFinanceFacade(),
      $this->config(),
    );
  }

  private function newCheckoutAddressReader(): CheckoutAddressReader
  {
    return new CheckoutAddressReader(
      $this->dependencyProvider()->shareOperationsUnitFacade(),
    );
  }

  private function newCheckoutChecksum(): CheckoutChecksum
  {
    return new CheckoutChecksum($this->shareCacheRepository());
  }

  private function newValidationMessagesMap(): ValidationMessagesMap
  {
    return new ValidationMessagesMapEN();
  }

  private function shareCheckoutRepository(): CheckoutReadWriteRepository
  {
    /** @var CheckoutReadWriteRepository */
    return $this->share(
      CheckoutReadWriteRepository::class,
      fn () => new CheckoutReadWriteRepository(
        $this->shareCheckoutReadRepository(),
        $this->shareCheckoutWriteRepository()
      )
    );
  }

  private function shareCheckoutWriteRepository(): CheckoutWriteRepository
  {
    /** @var CheckoutWriteRepository */
    return $this->share(
      CheckoutWriteRepository::class,
      fn () => new CheckoutWriteRepository($this->dependencyProvider()->shareDatabaseClient())
    );
  }

  private function shareCheckoutReadRepository(): CheckoutReadRepository
  {
    /** @var CheckoutReadRepository */
    return $this->share(
      CheckoutReadRepository::class,
      fn () => new CheckoutReadRepository(
        $this->dependencyProvider()->shareDatabaseClient(),
        $this->shareMapperFactory()
      )
    );
  }

  private function shareCacheRepository(): CheckoutCacheRepository
  {
    /** @var CheckoutCacheRepository */
    return $this->share(
      CheckoutCacheRepository::class,
      fn () => new CheckoutCacheRepository($this->dependencyProvider()->shareCacheStorageClient())
    );
  }

  private function shareMapperFactory(): MapperFactory
  {
    /** @var MapperFactory */
    return $this->share(
      MapperFactory::class,
      fn () => new MapperFactory()
    );
  }
}
