<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout\Request;

use Vijoni\Application\SchemaValidator\JsonSchemaValidator;
use Vijoni\Sales\Shared\OrderItem;
use Vijoni\Sales\Shared\ValidationMessagesMap;

class UpdateCartRequest
{
  private const SCHEMA_FILEPATH = __DIR__ . '/schema-update-cart.json';

  private string $payload = '';
  private string $customerId = '';

  public function __construct(
    private JsonSchemaValidator $jsonSchemaValidator,
    private ValidationMessagesMap $messagesMap
  ) {
  }

  public function use(string $customerId, string $payload): void
  {
    $this->payload = $payload;
    $this->customerId = $customerId;
  }

  public function validate(): array
  {
    return $this->jsonSchemaValidator->validate(
      $this->payload,
      self::SCHEMA_FILEPATH,
      $this->messagesMap->readMap()
    );
  }

  public function createOrderItem(): OrderItem
  {
    /** @var array $data */
    ['data' => $data ] = (array)json_decode($this->payload, true);

    $orderItem = new OrderItem();
    $orderItem->fromArray([
      OrderItem::PRODUCT_ID => $data['productId'],
      OrderItem::PRICE => $data['price'],
      OrderItem::COUNT => $data['count'],
    ]);

    return $orderItem;
  }

  public function getCustomerId(): string
  {
    return $this->customerId;
  }
}
