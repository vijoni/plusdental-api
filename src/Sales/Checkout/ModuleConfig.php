<?php

declare(strict_types=1);

namespace Vijoni\Sales\Checkout;

use Vijoni\Sales\Shared\Payment;
use Vijoni\Unit\BaseModuleConfig;

class ModuleConfig extends BaseModuleConfig
{
  public function getAvailablePaymentProviders(): array
  {
    return [
      Payment::PROVIDER_STRIPE_CREDIT_CARD => true,
      Payment::PROVIDER_SEPA => true,
      Payment::PROVIDER_ZAB_INSTALLMENTS => true,
    ];
  }

  public function getInstallmentsThreshold(): int
  {
    return 700; // eg. 700EUR
  }
}
