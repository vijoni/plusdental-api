<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Repository;

class MapperFactory
{
  public function newTaxRateMapper(): TaxRateMapper
  {
    return new TaxRateMapper();
  }
}
