<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance;

use Vijoni\Sales\Finance\Payment\PaymentFactory;
use Vijoni\Sales\Finance\Payment\SepaPayment;
use Vijoni\Sales\Finance\Payment\StripeCreditCardPayment;
use Vijoni\Sales\Finance\Payment\ZabInstallmentsPayment;
use Vijoni\Sales\Finance\Repository\MapperFactory;
use Vijoni\Sales\Finance\Service\SalesCalculator;
use Vijoni\Sales\Finance\Repository\FinanceReadRepository;
use Vijoni\Unit\BaseModuleFactory;

/**
 * @method ModuleDependencyProvider dependencyProvider()
 * @method ModuleConfig config()
 */
class ModuleFactory extends BaseModuleFactory
{
  public function shareSalesCalculator(): SalesCalculator
  {
    /** @var SalesCalculator */
    return $this->share(
      SalesCalculator::class,
      fn () => new SalesCalculator()
    );
  }

  public function newPaymentFactory(): PaymentFactory
  {
    return new PaymentFactory($this);
  }

  public function shareFinanceReadRepository(): FinanceReadRepository
  {
    /** @var FinanceReadRepository */
    return $this->share(
      FinanceReadRepository::class,
      fn () => new FinanceReadRepository(
        $this->dependencyProvider()->shareDatabaseClient(),
        $this->shareMapperFactory()
      )
    );
  }

  private function shareMapperFactory(): MapperFactory
  {
    /** @var MapperFactory */
    return $this->share(
      MapperFactory::class,
      fn () => new MapperFactory()
    );
  }

  public function newStripeCreditCardPayment(): StripeCreditCardPayment
  {
    return new StripeCreditCardPayment();
  }

  public function newSepaPayment(): SepaPayment
  {
    return new SepaPayment();
  }

  public function newZabInstallmentsPayment(): ZabInstallmentsPayment
  {
    return new ZabInstallmentsPayment($this->config());
  }
}
