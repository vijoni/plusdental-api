<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance;

use Vijoni\Sales\Finance\Payment\Zab\ZabPaymentRate;
use Vijoni\Unit\BaseModuleConfig;

class ModuleConfig extends BaseModuleConfig
{
  public function getZabRates(): array
  {
    return [
      '12' => new ZabPaymentRate(12, '5.45', '5.5881350'),
      '24' => new ZabPaymentRate(24, '5.45', '11.485841'),
      '36' => new ZabPaymentRate(36, '5.45', '17.719795'),
      '48' => new ZabPaymentRate(48, '5.45', '24.299140'),
      '60' => new ZabPaymentRate(60, '5.45', '31.2433'),
      '72' => new ZabPaymentRate(72, '5.45', '38.5775'),
    ];
  }

  public function getZabRateByTotalPayments(int $totalPayments): ZabPaymentRate
  {
    $zabRates = $this->getZabRates();
    $variant = (string)$totalPayments;

    if (!isset($zabRates[$variant])) {
      throw new \Exception("Unsupported zab rate: [{$totalPayments}]");
    }

    return $zabRates[$variant];
  }
}
