<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Payment\Zab;

class ZabPaymentRate
{
  public function __construct(
    private int $totalPayments,
    private string $apr,
    private string $effectiveApr
  ) {
  }

  public function getTotalPayments(): int
  {
    return $this->totalPayments;
  }

  public function getApr(): string
  {
    return $this->apr;
  }

  public function getEffectiveApr(): string
  {
    return $this->effectiveApr;
  }
}
