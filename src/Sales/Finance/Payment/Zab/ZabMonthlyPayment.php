<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Payment\Zab;

class ZabMonthlyPayment
{
  public function __construct(
    public string $interestValue,
    public string $remainingPrice,
    public string $paymentOffInvoice
  ) {
  }
}
