<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Payment;

use Vijoni\Sales\Finance\ModuleConfig;
use Vijoni\Sales\Finance\Payment\Zab\ZabFirstPayment;
use Vijoni\Sales\Finance\Payment\Zab\ZabMonthlyPayment;
use Vijoni\Sales\Finance\Payment\Zab\ZabPaymentRate;
use Vijoni\Sales\Shared\InstallmentPayment;
use Vijoni\Sales\Shared\Payment;
use Vijoni\Sales\Shared\Totals;

class ZabInstallmentsPayment extends Payment implements InstallmentPayment
{
  public function __construct(private ModuleConfig $financeConfig)
  {
    parent::__construct(Payment::TYPE_INSTALLMENTS, Payment::PROVIDER_ZAB_INSTALLMENTS);
  }

  /**
   * @param Totals $totals
   * @return Totals[]
   */
  public function calculateAllVariants(Totals $totals): array
  {
    $variants = [];
    foreach ($this->calculateAllInstallments($totals) as $months => $totals) {
      $variants[$months] = $totals;
    }

    return $variants;
  }

  public function calculateVariant(Totals $totals, string $variant): Totals
  {
    $zabPaymentRate = $this->financeConfig->getZabRateByTotalPayments(intval($variant));

    return $this->calculateInstallment($totals, $zabPaymentRate);
  }

  /**
   * @return Totals[]
   */
  private function calculateAllInstallments(Totals $totals): array
  {
    $zabRates = $this->financeConfig->getZabRates();
    $result = [];
    foreach ($zabRates as $zabPaymentRate) {
      $result[$zabPaymentRate->getTotalPayments()] = $this->calculateInstallment($totals, $zabPaymentRate);
    }

    return $result;
  }

  private function calculateInstallment(Totals $totals, ZabPaymentRate $paymentRate): Totals
  {
    $totalPayments = $paymentRate->getTotalPayments();

    $firstPayment = $this->calculateFirstPayment(
      $totals->getPriceToPay(),
      $paymentRate->getApr(),
      $paymentRate->getEffectiveApr()
    );

    $monthlyInstallment = $firstPayment->monthlyInterest;

    $allPayments = $this->calculateAllPayments(
      $firstPayment,
      $monthlyInstallment,
      $totalPayments,
      $paymentRate->getApr()
    );
    $lastPayment = end($allPayments);

    $totalPriceToPay = bcadd(bcmul($monthlyInstallment, (string)$totalPayments), $lastPayment->remainingPrice);

    $installmentTotals = new Totals();
    $installmentTotals->setPriceToPay(roundCents($totalPriceToPay));
    $installmentTotals->setPaymentRate($paymentRate->getApr());
    $installmentTotals->setTotalPayments($paymentRate->getTotalPayments());
    $installmentTotals->setSinglePaymentValue(roundCents($monthlyInstallment));

    return $installmentTotals;
  }

  private function calculateFirstPayment(string $priceToPay, string $apr, string $effectiveApr): ZabFirstPayment
  {
    $monthlyInterest = $this->calcMonthlyInterest($priceToPay, $apr);
    $paymentOffInvoiceAmount = bcdiv($monthlyInterest, bcdiv($effectiveApr, '100'));
    $remainingPriceToPay = $this->calcRemainingPriceToPay($priceToPay, $paymentOffInvoiceAmount);
    $monthlyInterest = bcadd($monthlyInterest, $paymentOffInvoiceAmount);

    return new ZabFirstPayment($monthlyInterest, $remainingPriceToPay);
  }

  private function calculateAllPayments(
    ZabFirstPayment $firstPayment,
    string $monthlyInterest,
    int $totalPayments,
    string $apr
  ): array {
    $previousPayment = $firstPayment;
    $payments = [$firstPayment];
    for ($i = $totalPayments - 1; $i > 0; --$i) {
      $previousPayment = $this->createPayment($previousPayment->remainingPrice, $apr, $monthlyInterest);
      $payments[] = $previousPayment;
    }

    if ($totalPayments === 48) {
      $payments = $this->process48Payments($apr, $monthlyInterest, $payments);
    }

    return $payments;
  }

  private function createPayment(string $basePrice, string $apr, string $monthlyInterest): ZabMonthlyPayment
  {
    $interestAmount = $this->calcMonthlyInterest($basePrice, $apr);
    $paymentOffInvoiceAmount = bcsub($monthlyInterest, $interestAmount);
    $remainingPriceToPay = $this->calcRemainingPriceToPay($basePrice, $paymentOffInvoiceAmount);

    return new ZabMonthlyPayment($interestAmount, $remainingPriceToPay, $paymentOffInvoiceAmount);
  }

  private function calcMonthlyInterest(string $priceToPay, string $apr): string
  {
    $monthlyInterestRate = bcdiv(bcdiv($apr, '12'), '100');

    return bcmul($priceToPay, $monthlyInterestRate);
  }

  private function calcRemainingPriceToPay(string $priceToPay, string $paymentOffInvoiceAmount): string
  {
    return bcsub($priceToPay, $paymentOffInvoiceAmount);
  }

  private function process48Payments(string $apr, string $monthlyInterest, array $allPayments): array
  {
    $lastIndex = count($allPayments) - 1;
    $secondToLastPayment = $allPayments[$lastIndex - 1];
    $allPayments[$lastIndex] = $this->createLast48Payment(
      $secondToLastPayment->remainingPrice,
      $apr,
      $monthlyInterest
    );

    return $allPayments;
  }

  private function createLast48Payment(string $basePrice, string $apr, string $monthlyInterest): ZabMonthlyPayment
  {
    $payment = $this->createPayment($basePrice, $apr, $monthlyInterest);
    $payment->remainingPrice = bcadd($payment->remainingPrice, bcsub($monthlyInterest, $basePrice));

    return $payment;
  }
}
