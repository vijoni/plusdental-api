<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Payment;

use Vijoni\Sales\Finance\ModuleFactory;
use Vijoni\Sales\Shared\Payment;

class PaymentFactory
{
  public function __construct(private ModuleFactory $financeFactory)
  {
  }

  /**
   * @param string[] $paymentProviders
   * @return Payment[]
   */
  public function createPayments(array $paymentProviders): array
  {
    $map = $this->paymentProvidersMap();

    $payments = [];
    foreach ($paymentProviders as $paymentProviderCode) {
      if (isset($map[$paymentProviderCode])) {
        $constructorFunction = $map[$paymentProviderCode];
        $payments[$paymentProviderCode] = $constructorFunction();
      }
    }

    return $payments;
  }

  private function paymentProvidersMap(): array
  {
    $financeFactory = $this->financeFactory;

    return [
      Payment::PROVIDER_STRIPE_CREDIT_CARD => fn () => $financeFactory->newStripeCreditCardPayment(),
      Payment::PROVIDER_SEPA => fn () => $financeFactory->newSepaPayment(),
      Payment::PROVIDER_ZAB_INSTALLMENTS => fn () => $financeFactory->newZabInstallmentsPayment(),
    ];
  }
}
