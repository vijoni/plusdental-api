<?php

declare(strict_types=1);

namespace Vijoni\Sales\Finance\Payment;

use Vijoni\Sales\Shared\Payment;

class SepaPayment extends Payment
{
  public function __construct()
  {
    parent::__construct(Payment::TYPE_ONETIME_PAYMENT, Payment::PROVIDER_SEPA);
  }
}
