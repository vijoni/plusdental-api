<?php

declare(strict_types=1);

namespace Vijoni\Sales\Order;

use Vijoni\Sales\Shared\Order;
use Vijoni\Unit\BaseModuleFacade;

/**
 * @method ModuleFactory moduleFactory()
 */
class ModuleFacade extends BaseModuleFacade
{
  public function createOrder(Order $order): bool
  {
    return $this->moduleFactory()->newCreateOrderUseCase()->createOrder($order);
  }

  public function tryMarkPurchaseDuplicate(string $key, string $value): bool
  {
    return $this->moduleFactory()->shareOrderCacheRepository()->tryMarkPurchaseDuplicate($key, $value);
  }
}
