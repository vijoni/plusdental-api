<?php

declare(strict_types=1);

namespace Vijoni\Sales\Order;

use Vijoni\Sales\Order\Repository\MapperFactory;
use Vijoni\Sales\Order\Repository\OrderCacheRepository;
use Vijoni\Sales\Order\Repository\OrderWriteRepository;
use Vijoni\Sales\Order\UseCase\CreateOrderUseCase;
use Vijoni\Unit\BaseModuleFactory;

/**
 * @method ModuleDependencyProvider dependencyProvider()
 * @method ModuleConfig config()
 */
class ModuleFactory extends BaseModuleFactory
{
  public function newCreateOrderUseCase(): CreateOrderUseCase
  {
    return new CreateOrderUseCase(
      $this->shareOrderWriteRepository()
    );
  }

  public function shareOrderCacheRepository(): OrderCacheRepository
  {
    /** @var OrderCacheRepository */
    return $this->share(
      OrderCacheRepository::class,
      fn () => new OrderCacheRepository($this->dependencyProvider()->shareCacheStorageClient())
    );
  }

  private function shareOrderWriteRepository(): OrderWriteRepository
  {
    /** @var OrderWriteRepository */
    return $this->share(
      OrderWriteRepository::class,
      fn () => new OrderWriteRepository(
        $this->dependencyProvider()->shareDatabaseClient(),
        $this->shareMapperFactory()
      )
    );
  }

  private function shareMapperFactory(): MapperFactory
  {
    /** @var MapperFactory */
    return $this->share(
      MapperFactory::class,
      fn () => new MapperFactory()
    );
  }
}
