<?php

declare(strict_types=1);

namespace Vijoni\Sales\Order\Repository;

use Vijoni\Database\Client\DatabaseClient;
use Vijoni\Sales\Shared\Address;
use Vijoni\Sales\Shared\OrderTotalsDbTable;
use Vijoni\Sales\Shared\Order;
use Vijoni\Sales\Shared\OrderDbTable;
use Vijoni\Sales\Shared\OrderItemDbTable;

class OrderWriteRepository
{
  public function __construct(private DatabaseClient $db, private MapperFactory $mapperFactory)
  {
    $db->useWriteConnection();
  }

  public function createOrder(Order $order): void
  {
    $this->db->startTransaction();
    try {
      $orderDbId = $this->storeOrder($order);
      $this->storeOrderTotals($order, $orderDbId);
      $this->storeOrderItems($order, $orderDbId);
      $this->storePayment($order, $orderDbId);
      $this->storeAddress($order, $order->getBillingAddress(), $orderDbId);
      $this->storeAddress($order, $order->getShippingAddress(), $orderDbId);
    } catch (\Throwable $e) {
      $order->setOrderId('');
      $this->db->rollback();
      throw $e;
    }

    $this->db->commit();
  }

  private function storeOrder(Order $order): string
  {
    $orderMapper = $this->mapperFactory->newOrderMapper();
    $orderProperties = $orderMapper->mapOrderToDbRow($order);

    $qb = $this->db->newQueryBuilder(
      'INSERT INTO sales_order (%lc) VALUES(%l?) RETURNING dbid, order_id;',
      [
        array_keys($orderProperties),
        array_values($orderProperties),
      ],
      [],
      "sales: create order; customer:[{$order->getCustomerId()}]"
    );

    /** @var array $resultRow */
    $resultRow = $this->db->querySingleRow($qb)->getValue();
    $order->setOrderId($resultRow[OrderDbTable::ORDER_ID]);

    return (string)$resultRow['dbid'];
  }

  private function storeOrderTotals(Order $order, string $orderDbId): void
  {
    $orderMapper = $this->mapperFactory->newOrderMapper();
    $orderTotals = $orderMapper->mapOrderToOrderTotalsDbRows($order, $orderDbId);

    foreach ($orderTotals as $totals) {
      $comment = sprintf(
        'sales: create order total; tax_rate:[%s], order_id:[%s]',
        $totals[OrderTotalsDbTable::TAX_RATE],
        $order->getOrderId(),
      );

      $qb = $this->db->newQueryBuilder(
        'INSERT INTO sales_order_totals (%lc) VALUES(%l?);',
        [
          array_keys($totals),
          array_values($totals),
        ],
        [],
        $comment
      );
      $this->db->query($qb);
    }
  }

  private function storeOrderItems(Order $order, string $orderDbId): void
  {
    $orderMapper = $this->mapperFactory->newOrderMapper();
    $orderItems = $orderMapper->mapOrderToOrderItemsDbRows($order, $orderDbId);

    foreach ($orderItems as $orderItem) {
      $comment = sprintf(
        'sales: create order items; product_id:[%s], order_id:[%s]',
        $orderItem[OrderItemDbTable::PRODUCT_ID],
        $order->getOrderId(),
      );

      $qb = $this->db->newQueryBuilder(
        'INSERT INTO sales_order_item (%lc) VALUES(%l?);',
        [
          array_keys($orderItem),
          array_values($orderItem),
        ],
        [],
        $comment
      );
      $this->db->query($qb);
    }
  }

  private function storePayment(Order $order, string $orderDbId): void
  {
    $orderMapper = $this->mapperFactory->newOrderMapper();
    $payment = $orderMapper->mapOrderToOrderPaymentDbRows($order, $orderDbId);

    $qb = $this->db->newQueryBuilder(
      'INSERT INTO sales_order_payment (%lc) VALUES(%l?);',
      [
        array_keys($payment),
        array_values($payment),
      ],
      [],
      "sales: create order payment; order_id:[{$order->getOrderId()}]",
    );

    $this->db->query($qb);
  }

  private function storeAddress(Order $order, Address $address, string $orderDbId): void
  {
    $orderMapper = $this->mapperFactory->newOrderMapper();
    $addressRow = $orderMapper->mapOrderAddressToAddressDbRow($address, $orderDbId);

    $qb = $this->db->newQueryBuilder(
      'INSERT INTO sales_order_address (%lc) VALUES(%l?);',
      [
        array_keys($addressRow),
        array_values($addressRow),
      ],
      [],
      "sales: create order address; type:[{$address->getType()}], order_id:[{$order->getOrderId()}]",
    );

    $this->db->query($qb);
  }
}
