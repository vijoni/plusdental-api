<?php

declare(strict_types=1);

namespace Vijoni\Sales\Order\Repository;

use Vijoni\Application\Redis\RedisClient;

class OrderCacheRepository
{
  private const KEY_PREFIX = 'sales_';
  private const KEY_DUPLICATE_PURCHASE = 'duplicate_purchase_';

  private const PURCHASE_DUPLICATE_TTL = 10;

  public function __construct(private RedisClient $redis)
  {
  }

  public function tryMarkPurchaseDuplicate(string $key, string $value): bool
  {
    return $this->redis->setNxEx(
      $this->buildKey(self::KEY_DUPLICATE_PURCHASE, $key),
      self::PURCHASE_DUPLICATE_TTL,
      $value
    );
  }

  private function buildKey(string $key, string $suffix = ''): string
  {
    return self::KEY_PREFIX . $key . $suffix;
  }
}
