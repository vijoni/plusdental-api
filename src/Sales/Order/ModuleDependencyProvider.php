<?php

declare(strict_types=1);

namespace Vijoni\Sales\Order;

use Vijoni\Application\DependencyProvider\CacheStorageClient;
use Vijoni\Application\DependencyProvider\DatabaseClient;
use Vijoni\Unit\BaseModuleDependencyProvider;
use Vijoni\Unit\DependencyProvider;

/**
 * @method DependencyProvider dependencyProvider()
 */
class ModuleDependencyProvider extends BaseModuleDependencyProvider
{
  use DatabaseClient;
  use CacheStorageClient;
}
