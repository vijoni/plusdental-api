<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

class CartItemDbTable
{
  public const PRODUCT_ID = 'product_id';
  public const CUSTOMER_ID = 'customer_id';
  public const PRICE = 'price';
  public const COUNT = 'count';
}
