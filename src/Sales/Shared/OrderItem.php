<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

use Vijoni\Sales\Shared\Generated\GeneratedOrderItem;

class OrderItem extends GeneratedOrderItem
{
  public function hasProductId(): bool
  {
    return $this->getProductId() !== '';
  }
}
