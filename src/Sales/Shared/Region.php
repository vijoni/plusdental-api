<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

class Region
{
  public function __construct(
    private string $countryCode,
    private string $locale
  ) {
  }

  public function getCountryCode(): string
  {
    return $this->countryCode;
  }

  public function getLocale(): string
  {
    return $this->locale;
  }
}
