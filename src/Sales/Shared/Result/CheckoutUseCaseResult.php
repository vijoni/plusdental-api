<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared\Result;

use Vijoni\Application\Result;

class CheckoutUseCaseResult extends Result
{
  private array $checkoutData = [];

  public function setCheckoutData(array $data): void
  {
    $this->checkoutData = $data;
  }

  public function getCheckoutData(): array
  {
    return $this->checkoutData;
  }
}
