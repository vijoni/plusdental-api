<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared\Result;

use Vijoni\Application\Result;

class PurchaseUseCaseResult extends Result
{
}
