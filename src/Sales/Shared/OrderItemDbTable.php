<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

class OrderItemDbTable
{
  public const ORDER_DBID = 'order_dbid';
  public const PRODUCT_ID = 'product_id';
  public const PARENT_PRODUCT_ID = 'parent_product_id';
  public const PRODUCT_TYPE = 'product_type';
  public const PRICE_TO_PAY = 'price_to_pay';
  public const TOTAL_PRICE_TO_PAY = 'total_price_to_pay';
  public const NET_PRICE = 'net_price';
  public const TAX_VALUE = 'tax_value';
  public const TAX_RATE = 'tax_rate';
  public const DISCOUNT = 'discount';
  public const COUNT = 'count';
  public const ORDERED_COUNT = 'ordered_count';
}
