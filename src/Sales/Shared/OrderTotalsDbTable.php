<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

class OrderTotalsDbTable
{
  public const ORDER_DBID = 'order_dbid';
  public const PRICE_TO_PAY = 'price_to_pay';
  public const NET_PRICE = 'net_price';
  public const TAX_VALUE = 'tax_value';
  public const TAX_RATE = 'tax_rate';
}
