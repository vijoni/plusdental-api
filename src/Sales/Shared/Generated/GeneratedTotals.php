<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared\Generated;

use Vijoni\ClassGenerator\ClassBase;

class GeneratedTotals extends ClassBase
{
  private string $priceToPay = '';
  private string $netPrice = '';
  private string $taxValue = '';
  private string $discountValue =  '0';
  private int $totalPayments =  -1;
  private string $paymentRate = '';
  private string $singlePaymentValue = '';

  public const PRICE_TO_PAY = 'priceToPay';
  public const NET_PRICE = 'netPrice';
  public const TAX_VALUE = 'taxValue';
  public const DISCOUNT_VALUE = 'discountValue';
  public const TOTAL_PAYMENTS = 'totalPayments';
  public const PAYMENT_RATE = 'paymentRate';
  public const SINGLE_PAYMENT_VALUE = 'singlePaymentValue';

  public function getPriceToPay(): string
  {
    return $this->priceToPay;
  }

  public function setPriceToPay(string $priceToPay): void
  {
    $this->priceToPay = $priceToPay;
    $this->modified[self::PRICE_TO_PAY] = true;
  }

  public function getNetPrice(): string
  {
    return $this->netPrice;
  }

  public function setNetPrice(string $netPrice): void
  {
    $this->netPrice = $netPrice;
    $this->modified[self::NET_PRICE] = true;
  }

  public function getTaxValue(): string
  {
    return $this->taxValue;
  }

  public function setTaxValue(string $taxValue): void
  {
    $this->taxValue = $taxValue;
    $this->modified[self::TAX_VALUE] = true;
  }

  public function getDiscountValue(): string
  {
    return $this->discountValue;
  }

  public function setDiscountValue(string $discountValue): void
  {
    $this->discountValue = $discountValue;
    $this->modified[self::DISCOUNT_VALUE] = true;
  }

  public function getTotalPayments(): int
  {
    return $this->totalPayments;
  }

  public function setTotalPayments(int $totalPayments): void
  {
    $this->totalPayments = $totalPayments;
    $this->modified[self::TOTAL_PAYMENTS] = true;
  }

  public function getPaymentRate(): string
  {
    return $this->paymentRate;
  }

  public function setPaymentRate(string $paymentRate): void
  {
    $this->paymentRate = $paymentRate;
    $this->modified[self::PAYMENT_RATE] = true;
  }

  public function getSinglePaymentValue(): string
  {
    return $this->singlePaymentValue;
  }

  public function setSinglePaymentValue(string $singlePaymentValue): void
  {
    $this->singlePaymentValue = $singlePaymentValue;
    $this->modified[self::SINGLE_PAYMENT_VALUE] = true;
  }

  public function toArray(): array
  {
    return [
      self::PRICE_TO_PAY => $this->getPriceToPay(),
      self::NET_PRICE => $this->getNetPrice(),
      self::TAX_VALUE => $this->getTaxValue(),
      self::DISCOUNT_VALUE => $this->getDiscountValue(),
      self::TOTAL_PAYMENTS => $this->getTotalPayments(),
      self::PAYMENT_RATE => $this->getPaymentRate(),
      self::SINGLE_PAYMENT_VALUE => $this->getSinglePaymentValue(),
    ];
  }

  public function toDeepArray(): array
  {
    return $this->toArray();
  }

  public function fromArray(array $properties): void
  {
    isset($properties[self::PRICE_TO_PAY]) && $this->setPriceToPay($properties[self::PRICE_TO_PAY]);
    isset($properties[self::NET_PRICE]) && $this->setNetPrice($properties[self::NET_PRICE]);
    isset($properties[self::TAX_VALUE]) && $this->setTaxValue($properties[self::TAX_VALUE]);
    isset($properties[self::DISCOUNT_VALUE]) && $this->setDiscountValue($properties[self::DISCOUNT_VALUE]);
    isset($properties[self::TOTAL_PAYMENTS]) && $this->setTotalPayments($properties[self::TOTAL_PAYMENTS]);
    isset($properties[self::PAYMENT_RATE]) && $this->setPaymentRate($properties[self::PAYMENT_RATE]);
    isset($properties[self::SINGLE_PAYMENT_VALUE]) && $this->setSinglePaymentValue($properties[self::SINGLE_PAYMENT_VALUE]);
  }

  public function fieldKeys(): array
  {
    return [
      self::PRICE_TO_PAY => true,
      self::NET_PRICE => true,
      self::TAX_VALUE => true,
      self::DISCOUNT_VALUE => true,
      self::TOTAL_PAYMENTS => true,
      self::PAYMENT_RATE => true,
      self::SINGLE_PAYMENT_VALUE => true,
    ];
  }
}
