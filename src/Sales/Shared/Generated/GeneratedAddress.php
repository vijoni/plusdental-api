<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared\Generated;

use Vijoni\ClassGenerator\ClassBase;

class GeneratedAddress extends ClassBase
{
  public const TYPE_SHIPPING = 'shipping';
  public const TYPE_BILLING = 'billing';

  private string $addressId = '';
  private string $firstname = '';
  private string $lastname = '';
  private string $phone = '';
  private string $street = '';
  private string $addressComplement = '';
  private string $city = '';
  private string $postCode = '';
  private string $countryCode = '';
  private string $type = '';

  public const ADDRESS_ID = 'addressId';
  public const FIRSTNAME = 'firstname';
  public const LASTNAME = 'lastname';
  public const PHONE = 'phone';
  public const STREET = 'street';
  public const ADDRESS_COMPLEMENT = 'addressComplement';
  public const CITY = 'city';
  public const POST_CODE = 'postCode';
  public const COUNTRY_CODE = 'countryCode';
  public const TYPE = 'type';

  public function getAddressId(): string
  {
    return $this->addressId;
  }

  public function setAddressId(string $addressId): void
  {
    $this->addressId = $addressId;
    $this->modified[self::ADDRESS_ID] = true;
  }

  public function getFirstname(): string
  {
    return $this->firstname;
  }

  public function setFirstname(string $firstname): void
  {
    $this->firstname = $firstname;
    $this->modified[self::FIRSTNAME] = true;
  }

  public function getLastname(): string
  {
    return $this->lastname;
  }

  public function setLastname(string $lastname): void
  {
    $this->lastname = $lastname;
    $this->modified[self::LASTNAME] = true;
  }

  public function getPhone(): string
  {
    return $this->phone;
  }

  public function setPhone(string $phone): void
  {
    $this->phone = $phone;
    $this->modified[self::PHONE] = true;
  }

  public function getStreet(): string
  {
    return $this->street;
  }

  public function setStreet(string $street): void
  {
    $this->street = $street;
    $this->modified[self::STREET] = true;
  }

  public function getAddressComplement(): string
  {
    return $this->addressComplement;
  }

  public function setAddressComplement(string $addressComplement): void
  {
    $this->addressComplement = $addressComplement;
    $this->modified[self::ADDRESS_COMPLEMENT] = true;
  }

  public function getCity(): string
  {
    return $this->city;
  }

  public function setCity(string $city): void
  {
    $this->city = $city;
    $this->modified[self::CITY] = true;
  }

  public function getPostCode(): string
  {
    return $this->postCode;
  }

  public function setPostCode(string $postCode): void
  {
    $this->postCode = $postCode;
    $this->modified[self::POST_CODE] = true;
  }

  public function getCountryCode(): string
  {
    return $this->countryCode;
  }

  public function setCountryCode(string $countryCode): void
  {
    $this->countryCode = $countryCode;
    $this->modified[self::COUNTRY_CODE] = true;
  }

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): void
  {
    $this->type = $type;
    $this->modified[self::TYPE] = true;
  }

  public function toArray(): array
  {
    return [
      self::ADDRESS_ID => $this->getAddressId(),
      self::FIRSTNAME => $this->getFirstname(),
      self::LASTNAME => $this->getLastname(),
      self::PHONE => $this->getPhone(),
      self::STREET => $this->getStreet(),
      self::ADDRESS_COMPLEMENT => $this->getAddressComplement(),
      self::CITY => $this->getCity(),
      self::POST_CODE => $this->getPostCode(),
      self::COUNTRY_CODE => $this->getCountryCode(),
      self::TYPE => $this->getType(),
    ];
  }

  public function toDeepArray(): array
  {
    return $this->toArray();
  }

  public function fromArray(array $properties): void
  {
    isset($properties[self::ADDRESS_ID]) && $this->setAddressId($properties[self::ADDRESS_ID]);
    isset($properties[self::FIRSTNAME]) && $this->setFirstname($properties[self::FIRSTNAME]);
    isset($properties[self::LASTNAME]) && $this->setLastname($properties[self::LASTNAME]);
    isset($properties[self::PHONE]) && $this->setPhone($properties[self::PHONE]);
    isset($properties[self::STREET]) && $this->setStreet($properties[self::STREET]);
    isset($properties[self::ADDRESS_COMPLEMENT]) && $this->setAddressComplement($properties[self::ADDRESS_COMPLEMENT]);
    isset($properties[self::CITY]) && $this->setCity($properties[self::CITY]);
    isset($properties[self::POST_CODE]) && $this->setPostCode($properties[self::POST_CODE]);
    isset($properties[self::COUNTRY_CODE]) && $this->setCountryCode($properties[self::COUNTRY_CODE]);
    isset($properties[self::TYPE]) && $this->setType($properties[self::TYPE]);
  }

  public function fieldKeys(): array
  {
    return [
      self::ADDRESS_ID => true,
      self::FIRSTNAME => true,
      self::LASTNAME => true,
      self::PHONE => true,
      self::STREET => true,
      self::ADDRESS_COMPLEMENT => true,
      self::CITY => true,
      self::POST_CODE => true,
      self::COUNTRY_CODE => true,
      self::TYPE => true,
    ];
  }
}
