<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

use Vijoni\Sales\Shared\Generated\GeneratedProductChild;

class ProductChild extends GeneratedProductChild
{
  public function clone(): static
  {
    return clone $this;
  }
}
