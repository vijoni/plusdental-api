<?php

declare(strict_types=1);

namespace Vijoni\Sales\Shared;

interface ValidationMessagesMap
{
  public function readMap(): array;
}
