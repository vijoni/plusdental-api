#!/usr/bin/env bash

sudo apt update
if ! hash jq 2>/dev/null; then
  sudo apt install -y jq vim curl git-crypt unzip
  sudo apt install -y ca-certificates apt-transport-https
fi

#install php
sudo apt install -y lsb-release software-properties-common gnupg2
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/sury-php.list
wget -qO - https://packages.sury.org/php/apt.gpg | sudo apt-key add -

sudo apt update
sudo apt install -y php8.1 \
php8.1-curl \
php8.1-bcmath \
php8.1-gd \
php8.1-mbstring \
php8.1-xdebug \
php8.1-xml \
php8.1-zip \
php8.1-yaml \
php8.1-pgsql \
php-dev \
php-pear

sudo cp /provision/xdebug.ini /etc/php/8.1/mods-available/

#install postgresql
echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -sc)-pgdg 14" | sudo tee /etc/apt/sources.list.d/pgdg.list
wget -qO - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

sudo apt update
sudo apt install -y postgresql postgresql-contrib
sudo systemctl enable postgresql.service
sudo systemctl start postgresql.service
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'postgres'"

# allow postgresql remote connections
grep -q "listen_addresses = '\*'" /etc/postgresql/13/main/postgresql.conf || sudo sed -i "/^#listen_addresses = 'localhost'/i listen_addresses = '*'" /etc/postgresql/13/main/postgresql.conf

#create database user
sudo grep -q vijoni_shop_admin /etc/postgresql/13/main/pg_hba.conf || printf "host\tall\t\t+vijoni_shop_admin\tall\t\t\tmd5\n" | sudo tee -a /etc/postgresql/13/main/pg_hba.conf
sudo grep -q vijoni_shop_user /etc/postgresql/13/main/pg_hba.conf || printf "host\tall\t\t+vijoni_shop_user\tall\t\t\tmd5\n" | sudo tee -a /etc/postgresql/13/main/pg_hba.conf
sudo -u postgres psql -tc "SELECT count(rolname) FROM pg_roles WHERE rolname = 'vijoni_shop_admin';" | grep -q '1' || sudo -u postgres psql -c "CREATE ROLE vijoni_shop_admin LOGIN SUPERUSER PASSWORD 'root'"
sudo -u postgres psql -tc "SELECT count(rolname) FROM pg_roles WHERE rolname = 'vijoni_shop_user';" | grep -q '1' || sudo -u postgres psql -c "CREATE ROLE vijoni_shop_user LOGIN PASSWORD 'root'"
sudo -u postgres createdb vijoni_shop --encoding=UTF8 --lc-collate=en_US.utf8 --lc-ctype=en_US.utf8 --owner=vijoni_shop_admin --template=template0
sudo -u postgres psql vijoni_shop -tc "ALTER SCHEMA public OWNER TO vijoni_shop_admin;"
sudo -u postgres psql vijoni_shop -tc "ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON TABLES TO vijoni_shop_user;"
sudo -u postgres psql vijoni_shop -tc "ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON SEQUENCES TO vijoni_shop_user;"
sudo -u postgres psql vijoni_shop -tc "ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON FUNCTIONS TO vijoni_shop_user;"

sudo systemctl restart postgresql

sudo grep -q DB_PRIVILEGED_USER /etc/environment || printf "DB_PRIVILEGED_USER=vijoni_shop_admin\n" | sudo tee -a /etc/environment
sudo grep -q DB_PRIVILEGED_PASSWORD /etc/environment || printf "DB_PRIVILEGED_PASSWORD=root\n" | sudo tee -a /etc/environment

#install java-jre
JAVA_RELEASE='8u191'
JAVA_VERSION='8.0_191'
sudo mkdir -p /opt/java/${JAVA_RELEASE}
sudo wget -O /opt/java/jre-${JAVA_RELEASE}-linux-x64.tar.gz https://javadl.oracle.com/webapps/download/AutoDL?BundleId=235717_2787e4a523244c269598db4e85c51e0c
sudo tar -xvzf /opt/java/jre-${JAVA_RELEASE}-linux-x64.tar.gz --directory /opt/java
sudo ln -s /opt/java/jre1.${JAVA_VERSION}/bin/java /usr/local/bin/java
sudo chown -R root:root /opt/java/jre1.${JAVA_VERSION}/

#install liquibase
LIQUIBASE_VERSION='4.9.1'
sudo mkdir -p /opt/liquibase/${LIQUIBASE_VERSION}
sudo wget -O /opt/liquibase/liquibase-${LIQUIBASE_VERSION}.tar.gz https://github.com/liquibase/liquibase/releases/download/v${LIQUIBASE_VERSION}/liquibase-${LIQUIBASE_VERSION}.tar.gz
sudo tar -xvzf /opt/liquibase/liquibase-${LIQUIBASE_VERSION}.tar.gz --directory /opt/liquibase/${LIQUIBASE_VERSION}
sudo ln -s /opt/liquibase/${LIQUIBASE_VERSION}/liquibase /usr/local/bin/liquibase
sudo chown -R root:root /opt/liquibase/${LIQUIBASE_VERSION}

#install nginx
sudo systemctl disable apache2
sudo systemctl stop apache2
wget -qO - https://nginx.org/keys/nginx_signing.key | sudo apt-key add -
echo "deb https://nginx.org/packages/mainline/debian/ $(lsb_release -sc) nginx" | sudo tee /etc/apt/sources.list.d/nginx.list
echo "deb-src https://nginx.org/packages/mainline/debian/ $(lsb_release -sc) nginx" | sudo tee -a /etc/apt/sources.list.d/nginx.list
sudo apt update
sudo apt install -y nginx
sudo mkdir -p /etc/nginx/sites-enabled
sudo cp /provision/nginx/sites-enabled/* /etc/nginx/sites-enabled/
sudo cp /provision/nginx/nginx.conf /etc/nginx/nginx.conf
sudo chmod -R u+rwX,g+rX,o-w /etc/nginx
sudo systemctl enable nginx.service
sudo systemctl restart nginx.service

#install php-fpm
sudo apt install -y php8.1-fpm
sudo cp -R /provision/php-fpm/* /etc/php/8.1/fpm/
sudo chmod -R u+rwX,g+rX,o-w /etc/php/8.1/fpm/
sudo systemctl enable php8.1-fpm.service
sudo systemctl restart php8.1-fpm.service

#install redis
sudo apt -y install redis-server
sudo systemctl enable redis-server.service
sudo pecl channel-update pecl.php.net
yes '' | sudo pecl install redis
sudo cp /provision/phpredis.ini /etc/php/8.1/mods-available/
sudo ln -s  /etc/php/8.1/mods-available/phpredis.ini /etc/php/8.1/fpm/conf.d/20-phpredis.ini
sudo ln -s  /etc/php/8.1/mods-available/phpredis.ini /etc/php/8.1/cli/conf.d/20-phpredis.ini
sudo systemctl restart redis-server.service
sudo systemctl restart php8.1-fpm.service

# provision the app
sudo -u vagrant -- sh -c "cd /app && XDEBUG_MODE=off ./composer install"

# add vagrant to www-data group
sudo usermod -G www-data vagrant
